# Memory Model

## Pointers

```cpp
int a = 5;
int *b = &a;
*b = 7;
// a = 5, *b = 5
cout << "a = " << a << ", *b = ", << *b << '\n';
// prints address of a
cout << b << '\n';
```

```cpp
void c_swap(int *, int *);
void cpp_swap(int&, int&);

int main() {
    int a = 5, b = 10;
    cpp_swap(&a, &b);
    cout << "a: " << a << " b: " << b << '\n';
}

void c_swap(int *x, int *y) {
    *x ^= *y;
    *y ^= *x;
    *x ^= *y;
}

void cpp_swap(int& x, int& y) {
    x ^= y;
    y ^= x;
    x ^= y;
}

```

- &
    - if used in declaration (incl function params), *creates and initializes* reference
    - if used outside of reference, fetches address of variable
        - ex: `ptr = &a;  // &a is rvalue, only can appear on right had side of assignment`
- \*
    - if used in declaration, *creates* pointer
        - ex: `int *p;`
    - if used outside declaration, dereferences pointer
        - ex: `*p = 3` `cout << *p;`

## Check Point

```cpp
int *p = &a;

// 1 <-- incorrect
int *p;
*p = &a;

// 2 <-- correct
int *p;
p = &a;
```

## Demo

```cpp
void foo(int *p) {
    *p = 500;
    return;
}

int main() {
    int val = 50;
    // ptr -> val
    int *ptr = &val;
    // ptr2 -> ptr -> val
    int **ptr2 = &ptr;

    cout << "val: " << val << '\n';
    cout << "*ptr: " << *ptr << '\n';
    cout << "**ptr2 " << **ptr2 << '\n';

    cout << "&val: " << &val << '\n';
    cout << "ptr: " << ptr << '\n';
    cout << "*ptr2 " << *ptr2 << '\n';

    cout << "&ptr: " << &ptr << '\n';
    cout << "ptr2 " << ptr2 << '\n';

    cout << "&ptr2 " << &ptr2 << '\n';

    
    *ptr = 100;
    // prints 100
    cout << " val: " <<  val << '\n';

    foo(&val);
    // prints 500
    cout << " val: " <<  val << '\n';
}
```

```cpp
int main() {
    /* references only one level deep max */
    int x = 5;
    // r -> x
    int& r = x;
    // r2 -> x
    int& r2 = r;
}
```

