# Big Three

if one implemented, probably need the rest

## Destructor

## Assignment Overload

```cpp
Object a;
Object b;

a = b;  // assignment overload called here
```

### Shallow Copy

- member-wise copy O(1)
- default behavior of `operator=`
- copies values of each member variable of b into a
- pointers to dynamic memory copied, point to same address
    - will be deleted in both destructors: double free
    - changes to heap memory in a affects b

### Deep Copy

- copy what each member var is pointing to
- duplicate any heap memory O(n)
- changing a doesn't affect b
- must be custom programmed (assigment operator overload)
    - return reference to calling obj (`*this`)

## Copy Constructor

- takes const reference
- defaults to shallow copy(?), deep copy must be implemented
- creates new distinct obj
- no return

```cpp
Object a;
Object b = a;  // copy contructor here
```

