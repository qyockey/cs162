#pragma once

#include "vehicle.hpp"

class Bus : public Vehicle {

private:
    int seat_capacity;

public:
    Bus(const std::string_view brand, const int year, const double mileage,
        const int seat_capacity);
    void print_info() const;
    double gas_price() const;

};

