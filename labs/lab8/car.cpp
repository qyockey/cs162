#include <iostream>

#include "car.hpp"


Car::Car(const std::string_view brand, const int year, const double mileage,
         const int num_doors, const bool electric)
  : Vehicle(brand, year, mileage), num_doors(num_doors), electric(electric) {
}


void Car::print_info() const {
    Vehicle::print_info();
    std::cout << "Number of Doors: " << num_doors << "\n"
                 "Electric: " << (electric ? "Yes" : "No") << "\n"
                 "Gas price: " << gas_price() << "\n";
}


double Car::gas_price() const {
    return mileage * (electric ? 0.05 : 0.35);
}


