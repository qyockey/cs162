#pragma once

#include "vehicle.hpp"

class Car : public Vehicle {

protected:
    const int num_doors;
    bool electric; // true if powered by electricity, false otherwise

public:
    Car(const std::string_view brand, const int year, const double mileage,
        const int num_doors, const bool electric);
    void print_info() const;
    double gas_price() const;
};

