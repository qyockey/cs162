#include "bot.hpp"

DeliveryBot::DeliveryBot(const std::string_view brand, const int year,
                         const double mileage, const int num_doors,
                         const bool electric)
  : Car(brand, year, mileage, num_doors, electric) {
}


void DeliveryBot::set_electic(bool is_electric) {
    this->electric = is_electric;
}

