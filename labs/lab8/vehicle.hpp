#pragma once

#include <string>

class Vehicle {

protected:
    const std::string brand;
    const int year;
    double mileage;

public:
    Vehicle(const std::string_view brand, const int year, const double mileage);
    virtual double gas_price() const;
    virtual void print_info() const;
};

