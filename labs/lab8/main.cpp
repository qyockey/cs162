#include "bot.hpp"
#include "bus.hpp"
#include "car.hpp"
#include "vehicle.hpp"

int main() {
    Vehicle semi("Daimler", 1996, 5.4);
    Car outback("Subaru", 2000, 23.1, 4, false);
    Bus vw ("Volkswagen", 1954, 12.3, 16);
    DeliveryBot bot("Starship", 2019, 40, 1, false);

    semi.print_info();
    outback.print_info();
    vw.print_info();
    bot.print_info();

    bot.set_electic(true);
    bot.print_info();

    return EXIT_SUCCESS;
}