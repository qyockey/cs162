#include <iostream>

using namespace std;

void function(int num, int ctr, int &r) {
  int i;
  // check all possible factors of num from 0 to num / 2
  for(i = 2;i <= num/2; i++){
    if(num % i==0){
      // number has factor
      ctr++;
      break;
    }
  }
  if(ctr == 0 && num != 1)
    // prime
    r = 1;
  
  else
    // not prime
    r = 0;
}

int main(){

  int num, ctr = 0, r = -1;
  cout << "Input a number: ";
  cin >> num;
  
  // check if prime
  function(num, ctr, r);
  cout << r << endl;;

  return 0;
}

