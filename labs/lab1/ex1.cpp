#include <iostream>

using namespace std; 

void function (int x[], int n) {
    int i, temp, j = n;
    bool swapped = true;

    while (swapped == true) {
        swapped = false;
        
        // sort array in ascending order
        for (i = 1; i < j; i++) {
            if (x[i] < x[i-1]) {

                // swap x[i] with x[i-1]
                temp = x[i];
                x[i] = x[i-1];
                x[i-1] = temp;
                
                swapped = true;
            }
        }
        j--;
    }
}
 
int main () {
    int x[] = {15, 56, 12, -21, 1, 659, 3, 83, 51, 3, 135, 0};
    int size = sizeof(x) / sizeof(x[0]); // figure out the size of the array
    
    // print array
    for (int i = 0; i < size; i++)
        cout << x[i] << " ";
    cout << endl;
    
    function(x, size);
    
    // print array
    for (int i = 0; i < size; i++)
        cout << x[i] << " ";
    cout << endl;
    
    return 0;
}

