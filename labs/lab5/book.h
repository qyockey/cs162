#ifndef BOOK_H
#define BOOK_H

#include <fstream>

struct Book {
    std::string name;
    int num_pages;
    std::string author;
    int year_published;
};


Book* create_books(int num_books);


void print_book(const Book& book, std::ofstream& output_file);


void populate_one_book(Book* books, int book_index, std::ifstream& book_file);


void delete_books(Book* books);


Book* parse_books(const std::string& filename);


void search_by_title(const Book* books, const int num_books,
                     std::ofstream& output_file);


void print_books_over_300_pages(const Book* books, const int num_books,
                                std::ofstream& output_file);


void sort_by_year(Book* books, const int num_books);


void quicksort_year(Book* books, int start, int end);


void swap_books(Book* books, int a, int b);


#endif

