#include <iostream>
#include <string>

#include "book.h"

#define NUM_OPTIONS 4
enum class Action {
    SEARCH_BY_TITLE,
    PRINT_BOOKS_OVER_300_PAGES,
    SORT_BY_YEAR,
    QUIT
};


int main() {
    std::ifstream book_file("input.txt", std::ios::in);
    if (!book_file.is_open()) {
        std::cerr << "Could not open file\n";
        exit(EXIT_FAILURE);
    }

    int num_books{};
    book_file >> num_books;
    Book* books {create_books(num_books)};

    for (int book_index {0}; book_index < num_books; book_index++) {
        populate_one_book(books, book_index, book_file);
    }

    book_file.close();

    std::ofstream output_file("output.txt", std::ios::out | std::ios::app);
    if (!output_file.is_open()) {
        std::cerr << "Could not open file\n";
        exit(EXIT_FAILURE);
    }

    const std::string menu_options[] {
        "Search by title",
        "Print books over 300 pages",
        "Sort by year",
        "Quit"
    };

    Action selected_action;
    do {
        for (int option{0}; option < NUM_OPTIONS; ++option) {
            std::cout << option + 1<< ") " << menu_options[option] << '\n';
        }
        std::cout << "\nEnter your choice: ";
        int selection{};
        std::cin >> selection;
        selected_action = static_cast<Action>(selection - 1);
        std::cout << '\n';
        
        switch (selected_action) {
            case Action::SEARCH_BY_TITLE:
                search_by_title(books, num_books, output_file);
                break;
            case Action::PRINT_BOOKS_OVER_300_PAGES:
                print_books_over_300_pages(books, num_books, output_file);
                break;
            case Action::SORT_BY_YEAR:
                sort_by_year(books, num_books);
                break;
            case Action::QUIT:
                break;
        }
    } while (selected_action != Action::QUIT);
}
