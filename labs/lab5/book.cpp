#include "book.h"
#include <fstream>
#include <iostream>


Book* create_books(int num_books) {
    return new Book[num_books];
}


void print_book(const Book& book, std::ofstream& output_file) {
    output_file << "Book Title: "      << book.name << "\n"
                   "Pages: "           << book.num_pages << "\n"
                   "Author Name: "     << book.author << "\n"
                   "Year of Release: " << book.year_published << "\n";
}


void populate_one_book(Book* books, int book_index, std::ifstream& book_file) {
    if (!books) {
        std::cerr << "Error: cannot populate null array.\n";
        exit(EXIT_FAILURE);
    }

    if (book_index < 0) {
        std::cerr << "Error: book index must be positive.\n";
        exit(EXIT_FAILURE);
    }

    Book& current_book {books[book_index]};
    book_file >> current_book.name;
    book_file >> current_book.num_pages;
    book_file >> current_book.author;
    book_file >> current_book.year_published;
}


void delete_books(Book* books) {
    if (!books) {
        std::cerr << "Error: cannot delete null array.\n";
        exit(EXIT_FAILURE);
    }
    delete[] books;
    books = nullptr;
}


Book* parse_books(const std::string& filename) {
    std::ifstream book_file(filename);
    if (!book_file.is_open()) {
        std::cerr << "Could not open file\n";
        exit(EXIT_FAILURE);
    }

    int num_books{};
    book_file >> num_books;
    Book* books {create_books(num_books)};

    for (int book_index {0}; book_index < num_books; book_index++) {
        populate_one_book(books, book_index, book_file);
    }

    book_file.close();

    return books;
}


void search_by_title(const Book* books, const int num_books,
                     std::ofstream& output_file) {
    std::string search_title;
    std::cout << "Enter the title to search for: ";
    std::cin >> search_title;

    bool found_title {false};
    for (int book_index {0}; book_index < num_books; book_index++) {
        const Book current_book {books[book_index]};
        if (current_book.name == search_title) {
            print_book(current_book, output_file);
            found_title = true;
        }
    }

    if (!found_title) {
        std::cout << "No books found with that title.\n";
    }
}


void print_books_over_300_pages(const Book *books, const int num_books,
                                std::ofstream& output_file) {
    bool found_match {false};
    for (int book_index {0}; book_index < num_books; book_index++) {
        const Book current_book {books[book_index]};
        if (current_book.num_pages > 300) {
            print_book(current_book, output_file);
            found_match = true;
        }
    }

    if (!found_match) {
        std::cout << "No books found over 300 pages.\n";
    }

}


void sort_by_year(Book *books, const int num_books) {
    quicksort_year(books, 0, num_books - 1);
}


void quicksort_year(Book *books, int start, int end) {

    // exit if one or zero elements to sort, already sorted
    if (start >= end) {
        return;
    }

    // year to compare against
    const int partition {(start + end) / 2};
    const int partition_year {books[partition].year_published};

    // switch partition to start
    swap_books(books, start, partition);

    // values to left are less than partition, to the right are greater
    int partition_boundary {start};

    // move values to left of boundary if greater than partition
    for (int index {partition_boundary + 1}; index <= end; ++index) {
        if (books[index].year_published > partition_year) {
            ++partition_boundary;
            swap_books(books, index, partition_boundary);
        }
    }

    // restore partiton at boundary
    swap_books(books, start, partition_boundary);

    // sort lower and upper subsets
    quicksort_year(books, start, partition_boundary - 1);
    quicksort_year(books, partition_boundary + 1, end);

}


void swap_books(Book *books, int a, int b) {
    Book temp {books[a]};
    books[a] = books[b];
    books[b] = temp;
}

