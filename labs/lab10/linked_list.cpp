#include "linked_list.h"
#include "node.h"

Linked_List::Linked_List() : head(nullptr), length(0) {
}

int Linked_List::get_length() const {
    return this->length;
}

void Linked_List::print() const {
    for (const Node* current {head}; current; current = current->next) {
        cout << ' ' << current->val;
    }
	cout << '\n';
	return;
}

void Linked_List::clear() {
    Node* current{head};
    while (current) {
        Node* next = current->next;
        delete current;
        current = next;
    }
    this->length = 0;

	return;
}

void Linked_List::push_front(const int val) {
    head = new Node(val, head);
    ++length;
	return;
}

void Linked_List::pop_front() {
    if (!head) {
        return;
    }

    Node* new_head {head->next};
    delete head;
    head = new_head;
    --length;
	return;
}

