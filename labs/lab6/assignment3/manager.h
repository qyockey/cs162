#ifndef MANAGER_H
#define MANAGER_H 

#include "airport.h"
#include <iostream>
#include <fstream>

using namespace std;

class Manager
{
private:
	int num_airports;	//number of airports
	Airport* a_arr;		//airport array

public:
	Manager();
	~Manager();

	void populate(ifstream& fin);

	int init();
	void print_menu();
	int get_menu_choice();
	
	void print_all();
	void check_flight_control();
	void add_flight_control();
	void cancel_flight_control();
	void take_off_control();
	void stats_control();

	
	void run();

	
	
};
#endif