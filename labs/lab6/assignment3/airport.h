#ifndef AIRPORT_H
#define AIRPORT_H 

#include <iostream>
#include <string>
#include <fstream>

#include "flight.h"

using namespace std;

class Airport
{
private:
	string name;		//airport name
	int num_flights;	//number of flights
	int cap;			//capacity
	Flight *f_arr;		//flight array

public:
	Airport();
	~Airport();

	void populate_airport(ifstream& fin);

	void add_a_flight(Flight& p);
	Flight remove_a_flight(int idx);

	void print_airport();
	
	
};
#endif