#include "manager.h"

using namespace std;


Manager::Manager(){
	
}

Manager::~Manager() {
	
}

void Manager::populate(ifstream& fin) {
	//Your code goes here:
	
	return; 
}

int Manager::init(){
	ifstream fin("airport.txt");
	//Error opening file?:
	//Your code goes here:


	Manager::populate(fin);
	return 1;
}

void Manager::print_menu(){
	cout << endl;
	cout << "1. View all Airports & Flights info" << endl;
	cout << "2. Check flight info" << endl;
	cout << "3. Add a new flight" << endl;
	cout << "4. Cancel a flight" << endl;
	cout << "5. Take off a flight" << endl;
	cout << "6. Print airport stats" << endl;
	cout << "7. Quit" << endl;

	cout << "Your choice: ";
}

int Manager::get_menu_choice() {
	int choice = 0;
	Manager::print_menu();
	cin >> choice;

	//Don't forget error handling!!!
	return choice;
}

void Manager::print_all(){
	//Your code goes here:
	
	return; 
}

void Manager::check_flight_control() {
	//Your code goes here:
	
	return; 
}

void Manager::add_flight_control() {
	//Your code goes here:

	return; 
}

void Manager::cancel_flight_control() {
	//Your code goes here:

	return;
}

void Manager::take_off_control() {
	//Your code goes here:
	
	return; 
}

void Manager::stats_control() {
	//Your code goes here:
	
	return; 
}

void Manager::run() {
	cout << endl;
	cout << "Welcome to Flight Manager!!" << endl;
	if (Manager::init() != 1)
		return;

	int choice = -1;
	while (choice != 7)
	{
		choice = Manager::get_menu_choice();

		//print all
		if (choice == 1) 
			Manager::print_all();

		//flight info
		else if (choice == 2){
			Manager::check_flight_control();
		}
		//add a new flight
		else if (choice == 3) {
			Manager::add_flight_control();
		}
		//cancel a flight
		else if (choice == 4) {
			Manager::cancel_flight_control();
		}
		//take off a flight
		if (choice == 5){
			Manager::take_off_control();
		}
		//airport stats
		else if (choice == 6) {
			Manager::stats_control();
		}
	}
	
	cout << "Bye!" << endl << endl;

	return;
}