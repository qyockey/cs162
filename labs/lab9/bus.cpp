#include <iostream>

#include "bus.hpp"


Bus::Bus(const std::string_view brand, const int year, const double mileage,
         const int seat_capacity)
  : Vehicle(brand, year, mileage), seat_capacity(seat_capacity) {
}


void Bus::print_info() const {
    Vehicle::print_info();
    std::cout << "Number of seats: " << seat_capacity << "\n"
                 "Gas price: " << gas_price() << "\n";
}


double Bus::gas_price() const {
    return mileage * 0.5;
}


