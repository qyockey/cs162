#include <string>
#include <iostream>

#include "vehicle.hpp"


Vehicle::Vehicle(const std::string_view brand, const int year,
                 const double mileage)
  : brand(brand), year(year), mileage(mileage) {
}


void Vehicle::print_info() const {
    std::cout << "\n"
                 "Brand: " << brand << "\n"
                 "Year: " << year << "\n"
                 "Mileage: " << mileage << "\n";
}

