#include <iostream>
#include <vector>

#include "bot.hpp"
#include "bus.hpp"
#include "car.hpp"
#include "vehicle.hpp"


void print_vehicle_info(const Vehicle* vehicle) {
    vehicle->print_info();
}


int main() {
    Car* outback = new Car("Subaru", 2000, 23.1, 4, false);
    Car* prius = new Car("Toyota", 2014, 34.2, 4, true);
    Car* fiat500 = new Car("Fiat", 2017, 27.7, 2, false);
    Car* model_x = new Car("Tesla", 2021, 9999, 4, true);
    Bus* vw_bus  = new Bus("Volkswagen", 1954, 12.3, 17);
    Bus* school_bus = new Bus("BlueBird", 1997, 5.6, 63);
    Bus* charter_bus = new Bus("BlueBird", 2009, 8.7, 71);
    DeliveryBot* normal_bot = new DeliveryBot("Starship", 2019, 40.0, 1, false);
    DeliveryBot* halloween_bot = new DeliveryBot("Starship", 2020, 40.0, 1, false);
    DeliveryBot* damaged_bot = new DeliveryBot("Starship", 2019, 13.4, 1, false);

    std::vector<Vehicle*> vehicles;
    vehicles.reserve(10);
    vehicles.resize(10);
    vehicles[0] = fiat500;
    vehicles[1] = vw_bus;
    vehicles[2] = normal_bot;
    vehicles[3] = halloween_bot;
    vehicles[4] = model_x;
    vehicles[5] = prius;
    vehicles[6] = charter_bus;
    vehicles[7] = damaged_bot;
    vehicles[8] = outback;
    vehicles[9] = school_bus;

    std::cout << "vector size: " << vehicles.size() << "\n"
                 "vector capacity: " << vehicles.capacity();

    for (auto const& vehicle : vehicles) {
        print_vehicle_info(vehicle);
        delete vehicle;
    }

    return EXIT_SUCCESS;
}

