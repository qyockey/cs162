#pragma once

#include <string>

class Vehicle {

protected:
    const std::string brand;
    const int year;
    double mileage;

public:
    Vehicle(const std::string_view brand, const int year, const double mileage);
    virtual ~Vehicle() = default;
    virtual double gas_price() const = 0;
    virtual void print_info() const;
};

