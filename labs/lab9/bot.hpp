#pragma once

#include "car.hpp"

class DeliveryBot : public Car {
public:
    DeliveryBot(const std::string_view brand, const int year,
                const double mileage, const int num_doors, const bool electric);
    void set_electic(bool is_electric);
};

