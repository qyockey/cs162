// This is for Lab 2 Part 3: Array exercise

#include <iostream>
#define ARRAY_SIZE_MIN 5
#define ARRAY_SIZE_MAX 50
#define ARRAY_ELEMENT_MAX 100

using namespace std;

void populate_array(int array[], const int size);
void print_array_stats(const int array[], const int size);
string read_string(const string_view& prompt);
int read_int(const string_view& prompt, const int min, const int max);
bool read_bool(const string_view& prompt);


// function to populate the array elements
void populate_array(int array[], const int size){
    for (int index {0}; index < size; ++index) {
        array[index] = rand() % ARRAY_ELEMENT_MAX + 1;
    }
	return;
}

void print_array_stats(const int array[], const int size) {
    int sum {0};
    int min {__INT_MAX__};
    int max {-1};
    int avg {};
    for (int index {0}; index < size; ++index) {
        const int element {array[index]};
        sum += element;
        if (element < min) {
            min = element;
        } else if (element > max) {
            max = element;
        }
    }
    avg = sum / size;
    cout << "\n"
            "Array Statistics\n"
            "================\n"
            "     Sum: " << sum << "\n"
            " Minimum: " << min << "\n"
            " Maximum: " << max << "\n"
            " Average: " << avg << "\n"
            "\n";
}

string read_string(const string_view& prompt) {
    cout << prompt;
    string input;
    getline(cin >> ws, input);
    if (cin.eof()) {
        throw ios_base::failure("EOF received, halting execution");
    }
    return input;
}

int read_int(const string_view& prompt, const int min, const int max) {
    while (true) {
        string input {read_string(prompt)};
        try {
            const int value {stoi(input)};
            if (to_string(value) != input) {
                throw invalid_argument {""};
            }
            if (min <= value && value <= max) {
                return value;
            }
            throw out_of_range {""};
        } catch (invalid_argument&) {
            cout << "Value must be an integer. Try again.\n";
        } catch (out_of_range&) {
            cout << "Value is out of range. Enter a value from "
                 << min << " to " << max << ".\n";
        }
    }
}

bool read_bool(const string_view& prompt) {
    while (true) {
        if (string input {read_string(prompt)}; input.length() == 1) {
            const char first_char {input.at(0)};
            if (first_char == 'y') {
                return true;
            } else if (first_char == 'n') {
                return false;
            }
        }
        cout << "Value must be 'y' or 'n'. Try again.\n";
    }
}

int main() {
    srand(static_cast<unsigned>(time(nullptr)));
    do {
        int array_size {read_int("Enter array size: ", ARRAY_SIZE_MIN,
                                 ARRAY_SIZE_MAX)};
        int *array {new int[array_size]};
        populate_array(array, array_size);
        print_array_stats(array, array_size);
    } while(read_bool("Would you like to restart the program? [y/n] "));
	
	return 0;
}
