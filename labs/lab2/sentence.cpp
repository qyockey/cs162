#include <iostream>
#include <string>

using namespace std;

/* Questions:
 *
 * 1) To change the contents of sentence with the modified function prototype,
 *        *s should be accessed instead of s.
 * 2) getline() needs to take *s as the buffer to store to instead of s.
 * 3) An ampersand in front of a parameter means that the parameter is a
 *        reference variable, whereas an asterisk in front of the parameter
 *        means it is a pointer.
 */

void get_sentence(string *s) {
    cout << "Enter a sentence: ";
    getline(cin, *s);
}

int main() {
    string sentence;

    get_sentence(&sentence);
    cout << sentence << endl;

    return 0;
}
