/* CS 162- Lab 1 - Q.2
 * Solution description: call the function foo using "reference" to see the values before and after the function
 */
 
#include <iostream>

using namespace std;

int foo(int* a, int& b, int c) {
    /*Set a to double its original value*/
    *a <<= 1;

    /*Set b to half its original value*/
    b >>= 1;

    /*Assign a+b to c*/
    c = *a + b;

    /*Return c*/
    return c;
}

int main() {
    /*Declare three integers x,y and z and initialize them to 7, 8, 9 respectively*/
    int x {7};
    int y {8};
    int z {9};
    
    /*Print the values of x, y and z*/
    cout << "x = " << x << '\n';
    cout << "y = " << y << '\n';
    cout << "z = " << z << '\n';

    /*Call foo() appropriately, passing x,y,z as parameters*/
    int foo_result {foo(&x, y, z)};

    /*Print the value returned by foo*/
    cout << "result of foo() = " << foo_result << '\n';

    /*Print the values of x, y and z again*/
    cout << "x = " << x << '\n';
    cout << "y = " << y << '\n';
    cout << "z = " << z << '\n';

    /*Is the return value different than the value of z?  Why? */
    /* return value equals 2x + y/2, not z */

    return 0;
}

