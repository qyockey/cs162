#include <iostream>
#include <iomanip>

using namespace std;

struct multdiv_entry {
    int mult;
    float div;
};


// function prototypes
multdiv_entry** create_table(int row, int col);
void print_table(multdiv_entry** tables, int row, int col);
void delete_table(multdiv_entry** tables, int row);


multdiv_entry** create_table(int row, int col) {
    multdiv_entry** table = new multdiv_entry*[row];
    for (int i {0}; i < row; ++i) {
        table[i] = new multdiv_entry[col];
        for (int j {0}; j < col; ++j) {
            table[i][j] = {
                .mult = (i + 1) * (j + 1),
                .div = static_cast<float>(i + 1) / (j + 1)
            };
        }
    }
    return table;
}


void print_table(multdiv_entry** tables, int row, int col) {

    cout << "Multiplication table:\n";
    for (int i {0}; i < row; ++i) {
        for (int j {0}; j < col; ++j) {
            cout << tables[i][j].mult << '\t';
        }
        cout << '\n';
    }
    cout << '\n';

    cout << "Division table:\n";
    for (int i {0}; i < row; ++i) {
        for (int j {0}; j < col; ++j) {
            cout << fixed << setprecision(2) << tables[i][j].div << '\t';
        }
        cout << '\n';
    }
    cout << '\n';
}


void delete_table(multdiv_entry** tables, int row) {
    for (int i {0}; i < row; ++i) {
        delete[] tables[i];
        tables[i] = nullptr;
    }
    delete[] tables;
    tables = nullptr;
}


int main() {
    int num_rows {};
    int num_cols {};

    int repeat {};
    do {
        cout << "Enter number of rows: ";
        cin >> num_rows;
        cout << "Enter number of columns: ";
        cin >> num_cols;

        multdiv_entry** table = create_table(num_rows, num_cols);
        print_table(table, num_rows, num_cols);
        delete_table(table, num_rows);
        table = nullptr;

        cout << "Would you like to generate a new table? [0-No, 1-Yes] ";
        cin >> repeat;
    } while (repeat != 0);

    return 0;
}

