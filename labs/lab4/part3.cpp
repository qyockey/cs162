// take an int argument, represent the size of the array,
// and return the address of the array
int* create_array1(int size) {
    if (size <= 0) {
        std::cerr << "Invalid size. Array size must be positive";
        return nullptr;
    }
    return new int[size];
}

// take a reference to an int pointer to allocate the array,
// and an int argument, represent the size of the array
void create_array2(int *& array, int size) {
    if (size <= 0) {
        std::cerr << "Invalid size. Array size must be positive";
        array = nullptr;
    }
    array = new int[size];
}

// take the address of an int pointer, dereference it to allocate,
// and an int argument, represent the size of the array
void create_array3 (int ** array, int size) {
    if (size <= 0) {
        std::cerr << "Invalid size. Array size must be positive";
        *array = nullptr;
    }
    *array = new int[size];
}

