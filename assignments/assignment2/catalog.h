#ifndef CATALOG_H
#define CATALOG_H

#include <iostream>
#include <fstream>
#include <string>

#include "output.h"

using namespace std;

// a struct to hold info of a Playlist
struct Playlist {
    string name;           // name of the playlist, one word
    int num_songs;         // number of songs in the playlist
    struct Song *songs;    // an array that holds all songs in the playlist
    float total_duration;  // total duration of the playlist
};

// a struct to hold info of a Song
struct Song {
    string title;       // title of the song, one word
    string artist;      // name of the artist, one word
    int year_release;   // the year of release
    float duration;     // duration of the song
    string genre;       // genre of the song, one word
    string restriction; // "E" or "none" for restriction level <-- should be
                        //    bool or struct
};


/**************************************************
 * Name: create_playlists()
 * Description: This function will dynamically allocate
 *              an array of playlists (of the requested size)
 * Parameters: num_playlists - size of the array
 * Pre-conditions: none
 * Post-conditions: a Playlist array of requested size is created and return
 ***********************************************/
Playlist* create_playlists(int num_playlists);


/**************************************************
 * Name: populate_one_list()
 * Description: This function will fill a single playlist struct 
 *              with information that is read in from the file
 * Parameters:  playlists - pointer to the Playlist array
 *              playlist_index - index of the Playlist in the array to be filled 
 *              playlist_file - input file to get the Playlist information
 * Pre-conditions: Playlist array has been allocated; 
 *               provided index is less than the array size
 * Post-conditions: a Playlist at provided index is populated
 ***********************************************/
void populate_one_list(Playlist* playlists, int playlist_index,
		ifstream& playlist_file); 


/**************************************************
 * Name: create_songs()
 * Description: This function will dynamically allocate
 *              an array of songs (of the requested size)
 * Parameters: num_songs - size of the array
 * Pre-conditions: none
 * Post-conditions: a Song array of requested size is created and return
 ***********************************************/
Song* create_songs(int num_songs);


/**************************************************
 * Name: populate_one_song()
 * Description: This function will fill a single song struct 
 *              with information that is read in from the file
 * Parameters:  song_arraay - pointer to the Song array
 *              song_index - index of the Song in the array to be filled 
 *              playlist_file - input file to get the Song information
 * Pre-conditions: Song array has been allocated; 
 *               provided index is less than the array size
 * Post-conditions: a Song at provided index is populated
 ***********************************************/
void populate_one_song(Song* song_array, int song_index,
		ifstream& playlist_file); 


/**************************************************
 * Name: delete_info()
 * Description: This function will delete all the memory that was
 *             dynamically allocated
 * Parameters: playlists - the Playlist array
 *             num_playlists - number of playlists in playlists
 * Pre-conditions: the provided Playlist array hasn't been freed yet
 * Post-conditions: the Playlist array, with all Songs inside, is freed
 ***********************************************/
void delete_info(Playlist* playlists, int num_playlists);


/**************************************************
 * Name: populate_playlists()
 * Description: This function will fill an array of playlists with
 *              information that is read in from the file.
 * Parameters:  playlists - pointer to the Playlist array
 *              num_playlists - reference to populate with number of playlists
 *              playlist_file - input file to get the Playlist information
 * Pre-conditions: N/A
 * Post-conditions: playlists contains allocated array of fully allocated
 *              playlist structs and num_playlists contains size of the
 *              playlist array
 ***********************************************/
void populate_playlists(Playlist*& playlists, int& num_playlists,
                        ifstream& playlist_file);


/**************************************************
 * Name: output_thin_separator()
 * Description: This function will output a thin separator row to
 *              separate blocks of text
 * Parameters: output - output stream to separator to
 * Pre-conditions: N/A
 * Post-conditions: separator written to output
 ************************************************/
void output_thin_separator(Output& output);


/**************************************************
 * Name: output_thick_separator()
 * Description: This function will output a thick separator row to
 *              separate blocks of text
 * Parameters: output - output stream to separator to
 * Pre-conditions: N/A
 * Post-conditions: separator written to output
 ************************************************/
void output_thick_separator(Output& output);


/**************************************************
 * Name: output_song()
 * Description: This function will write info of a song to output,
 *              unless it is age restricted
 * Parameters: song - pointer to song to print
 *             age_restricted - whether the user has age restrictions
 *             output - output stream to write song info to
 * Pre-conditions: all fields of song have been initialized
 * Post-conditions: song info has been written to output
 ***********************************************/
void output_song(const Song& song, const bool age_restricted, Output& output);


/**************************************************
 * Name: output_all_songs()
 * Description: This function will print info for each song in each
 *             playlist in playlists to the screen
 * Parameters: playlists - array of playlists to print
 *             num_playlists - number of playlists in playlists
 *             age_restricted - whether the user has age restrictions
 *             output - output stream to write song info to
 * Pre-conditions: all fields of all songs in each playlist have been
 *             initialized
 * Post-conditions: all song info has been written to output
 ***********************************************/
void output_all_songs(const Playlist* playlists, const int num_playlists,
                     const bool age_restricted, Output& output);


/**************************************************
 * Name: search_by_title()
 * Description: This function will prompt the user for a title to
 *             search for and then output the info of each song
 *             matching the provided title
 * Parameters: playlists - array of playlists to print
 *             num_playlists - number of playlists in playlists
 *             age_restricted - whether the user has age restrictions
 *             output - output stream to write playlist durations to
 * Pre-conditions: title fields of all songs in each playlist have been
 *             initialized
 * Post-conditions: song info of each song matching provided title has
 *             been written to output
 ***********************************************/
void search_by_title(const Playlist* playlists, const int num_playlists,
                     const bool age_restricted, Output& output);


/**************************************************
 * Name: search_by_genre()
 * Description: This function will prompt the user for a genre to
 *             search for and then output the info of each song
 *             matching the provided genre
 * Parameters: playlists - array of playlists to print
 *             num_playlists - number of playlists in playlists
 *             age_restricted - whether the user has age restrictions
 *             output - output stream to write playlist durations to
 * Pre-conditions: genre fields of all songs in each playlist have been
 *             initialized
 * Post-conditions: song info of each song matching provided genre has
 *             been written to output
 ***********************************************/
void search_by_genre(const Playlist* playlists, const int num_playlists,
                     const bool age_restricted, Output& output);


/**************************************************
 * Name: output_playlist_durations()
 * Description: This function will output the duration of each playlist
 *             in playlists
 * Parameters: playlists - array of playlists to print
 *             num_playlists - number of playlists in playlists
 *             output - output stream to write playlist durations to
 * Pre-conditions: total_duartion field of each playlist has been
 *             initialized
 * Post-conditions: duration of each playlist has been written to
 *             output
 ***********************************************/
void output_playlist_durations(const Playlist* playlists,
                               const int num_playlists, Output& output);


/**************************************************
 * Name: sort_playlist_songs_by_year()
 * Description: This function will sort the songs of each playlist
 *             sorted newest to oldest
 * Parameters: playlists - array of playlists to print
 *             num_playlists - number of playlists in playlists
 *             age_restricted - whether the user has age restrictions
 *             output - output stream to write playlist durations to
 * Pre-conditions: all fields of all songs in each playlist have been
 *             initialized
 * Post-conditions: each playlist sorted separately in descending order
 ************************************************/
void sort_playlist_songs_by_year(const Playlist* playlists,
                                 const int num_playlists);


/**************************************************
 * Name: swap_songs()
 * Description: This function will swap the position of two songs in
 *             a given song array
 * Parameters: songs - array containing song objects
 *             a - index of one song to be swapped
 *             b - index of other song to be swapped
 * Pre-conditions: all fields of all objects in songs have been
 *             initialized, 0 <= a, b < sizeof(songs) / sizeof(Song)
 * Post-conditions: song objects at indicies a and b have been swapped
 ************************************************/
void swap_songs(Song* songs, const int a, const int b);


/**************************************************
 * Name: quicksort_year()
 * Description: This function will sort a song array from newest to oldest
 * Parameters: songs - array of song objects to sort
 *             start - index to start sorting
 *             end - index to stop sorting
 * Pre-conditions: all fields of all songs in each playlist have been
 *             initialized, 0 <= start <= end < sizeof(songs) / sizeof(Song)
 * Post-conditions: elements of songs from start to end sorted from
 *             newest to oldest
 ************************************************/
void quicksort_year(Song* songs, const int start, const int end);


#endif

