#include <fstream>
#include <iostream>
#include <string>

#include "output.h"

using namespace std;


Output::Output() = default;


Output::Output(const string& output_filename) {
    open_file(output_filename);
}


Output::~Output() {
    if (file_output) {
        close_file();
    }
}


Output& Output::open_file(const string_view& filename) {
    file_output = true;
    output_filename = filename;

    output_file.open(output_filename, ios::out | ios::app);
    if (!output_file.is_open() || output_file.fail()) {
        cerr << "Error opening output file\n";
        exit(EXIT_FAILURE);
    }

    return *this;
}


void Output::close_file() {
    if (file_output) {
        file_output = false;
        output_file.close();
        if (output_file.is_open() || output_file.fail()) {
            cerr << "Error closing output file\n";
            exit(EXIT_FAILURE);
        }
    }
}


Output& Output::sync() {
    if (file_output) {
        output_file.flush();
    }
    return *this;
}


Output& Output::write(const string_view& data) {
    if (file_output) {
        output_file << data;
    } else {
        cout << data;
    }
    return *this;
}


Output& Output::write(const string& data) {
    if (file_output) {
        output_file << data;
    } else {
        cout << data;
    }
    return *this;
}


Output& Output::write(const char* data) {
    if (file_output) {
        output_file << data;
    } else {
        cout << data;
    }
    return *this;
}


Output& Output::write(const char data) {
    if (file_output) {
        output_file << data;
    } else {
        cout << data;
    }
    return *this;
}


Output& Output::write(const int data) {
    if (file_output) {
        output_file << data;
    } else {
        cout << data;
    }
    return *this;
}


Output& Output::write(const float data) {
    if (file_output) {
        output_file << data;
    } else {
        cout << data;
    }
    return *this;
}


Output& Output::operator<<(const string_view& data) { return write(data); }


Output& Output::operator<<(const string& data) { return write(data); }


Output& Output::operator<<(const char* data) { return write(data); }


Output& Output::operator<<(const char data) { return write(data); }


Output& Output::operator<<(const int data) { return write(data); }


Output& Output::operator<<(const float data) { return write(data); }

