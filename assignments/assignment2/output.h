#ifndef OUTPUT_H
#define OUTPUT_H

#include <fstream>
#include <iostream>
#include <string>

class Output {

private:
    bool file_output {false};
    std::string output_filename {};
    std::ofstream output_file {};

public:
    /**************************************************
     * Name: Output()
     * Description: Constructs an Output object that defaults to outputing
     *              to the screen.
     * Parameters: N/A
     * Pre-conditions: N/A
     * Post-conditions: file_output is set to false. output_filename is set
     *              to an empty string. output_file is set to an empty ofstream.
     ************************************************/
    Output();
    

    /**************************************************
     * Name: Output()
     * Description: Constructs an Output object that defaults to outputing
     *              to a file. If the file does not exist, it is created.
     *              If the file does exist, it is opened for appending. If the
     *              file cannot be opened, program execution terminates.
     * Parameters: output_filename - name of the file to output to
     * Pre-conditions: output_filename is a valid filename
     * Post-conditions: file_output is set to true. output_filename is set
     *              to output_filename. output_file is an ofstream object for
     *              output_filename opened for appending.
     ************************************************/
    explicit Output(const std::string& output_filename);


    /**************************************************
     * Name: ~Output()
     * Description: If file_output is true, output_file is closed. If
     *              output_file cannot be closed, program execution terminates.
     * Parameters: N/A
     * Pre-conditions: If file_output is true, then output_file is open.
     * Post-conditions: All memory allocated to this object is freed.
     ************************************************/
    ~Output();


    /**************************************************
     * Name: open_file()
     * Description: Output method set to file. If the file does not exist, it
     *              is created. If the file does exist, it is opened for
     *              appending. If the file cannot be opened, program execution
     *              terminates.
     * Parameters: output_filename - name of the file to output to
     * Pre-conditions: output_filename is a valid filename
     * Post-conditions: file_output is set to true. this->output_filename is set
     *              to output_filename. output_file is an ofstream object for
     *              output_filename opened for appending.
     ************************************************/
    Output& open_file(const std::string_view& output_filename);


    /**************************************************
     * Name: close_file()
     * Description: output_file is closed. If output_file cannot be closed,
     *              program execution terminates.
     * Parameters: N/A
     * Pre-conditions: output_file is opened.
     * Post-conditions: output_file is closed.
     ************************************************/
    void close_file();


    /**************************************************
     * Name: sync()
     * Description: If file_output is true, output_file is flushed.
     * Parameters: N/A
     * Pre-conditions: If file_output is true, then output_file is opened.
     * Post-conditions: output_file is flushed.
     ************************************************/
    Output& sync();

    
    /**************************************************
     * Name: write()
     * Description: Write string_view data to output.
     * Parameters: data - data to write.
     * Pre-conditions: If file_output is true, then output_file is opened.
     * Post-conditions: data is written to output.
     ************************************************/
    Output& write(const std::string_view& data);


    /**************************************************
     * Name: write()
     * Description: Write string data to output.
     * Parameters: data - data to write.
     * Pre-conditions: If file_output is true, then output_file is opened.
     * Post-conditions: data is written to output.
     ************************************************/
    Output& write(const std::string& data);


    /**************************************************
     * Name: write()
     * Description: Write char* data to output.
     * Parameters: data - data to write.
     * Pre-conditions: If file_output is true, then output_file is opened.
     * Post-conditions: data is written to output.
     ************************************************/
    Output& write(const char* data);


    /**************************************************
     * Name: write()
     * Description: Write char data to output.
     * Parameters: data - data to write.
     * Pre-conditions: If file_output is true, then output_file is opened.
     * Post-conditions: data is written to output.
     ************************************************/
    Output& write(const char data);
    

    /**************************************************
     * Name: write()
     * Description: Write int data to output.
     * Parameters: data - data to write.
     * Pre-conditions: If file_output is true, then output_file is opened.
     * Post-conditions: data is written to output.
     ************************************************/
    Output& write(const int data);


    /**************************************************
     * Name: write()
     * Description: Write float data to output.
     * Parameters: data - data to write.
     * Pre-conditions: If file_output is true, then output_file is opened.
     * Post-conditions: data is written to output.
     ************************************************/
    Output& write(const float data);


    /**************************************************
     * Name: operator<<()
     * Description: Write string_view data to output.
     * Parameters: data - data to operator<<.
     * Pre-conditions: If file_output is true, then output_file is opened.
     * Post-conditions: data is written to output.
     ************************************************/
    Output& operator<<(const std::string_view& data);


    /**************************************************
     * Name: operator<<()
     * Description: Write string data to output.
     * Parameters: data - data to operator<<.
     * Pre-conditions: If file_output is true, then output_file is opened.
     * Post-conditions: data is written to output.
     ************************************************/
    Output& operator<<(const std::string& data);


    /**************************************************
     * Name: operator<<()
     * Description: Write char* data to output.
     * Parameters: data - data to operator<<.
     * Pre-conditions: If file_output is true, then output_file is opened.
     * Post-conditions: data is written to output.
     ************************************************/
    Output& operator<<(const char* data);


    /**************************************************
     * Name: operator<<()
     * Description: Write char data to output.
     * Parameters: data - data to operator<<.
     * Pre-conditions: If file_output is true, then output_file is opened.
     * Post-conditions: data is written to output.
     ************************************************/
    Output& operator<<(const char data);
    

    /**************************************************
     * Name: operator<<()
     * Description: Write int data to output.
     * Parameters: data - data to operator<<.
     * Pre-conditions: If file_output is true, then output_file is opened.
     * Post-conditions: data is written to output.
     ************************************************/
    Output& operator<<(const int data);


    /**************************************************
     * Name: operator<<()
     * Description: Write float data to output.
     * Parameters: data - data to operator<<.
     * Pre-conditions: If file_output is true, then output_file is opened.
     * Post-conditions: data is written to output.
     ************************************************/
    Output& operator<<(const float data);
};


#endif

