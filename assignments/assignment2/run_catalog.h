#ifndef RUN_CATALOG_H
#define RUN_CATALOG_H

#include <iostream>
#include <fstream>

#include "catalog.h"
#include "output.h"

enum class OutputOption {
    SCREEN = 1,
    FILE   = 2
};


enum class Action {
    OUTPUT_ALL_SONGS          = 1,
    SEARCH_BY_TITLE           = 2,
    SEARCH_BY_GENRE           = 3,
    OUTPUT_PLAYLIST_DURATIONS = 4,
    SORT_BY_YEAR              = 5,
    QUIT                      = 6
};


/**************************************************
 * Name: open_input_file()
 * Description: This function will prompt the user for the name of a file to
 *              read playlist data from and will open the file. If the file
 *              cannot be opened, program execution will terminate.
 * Parameters: input_file - target to store opened input file stream to
 * Pre-conditions: N/A
 * Post-conditions: input_file contains file successfully opened for reading
 ***********************************************/
void open_input_file(std::ifstream& input_file);


/**************************************************
 * Name: prompt_output_method()
 * Description: This function will prompt the user whether they want to output
 *              to the screen or to a file. If they choose file output, they
 *              will be prompted for the name of the file, and the file will
 *              be opened for appending. If the file cannot be opened, program
 *              execution will terminate. 
 * Parameters: output - target to store opened output stream to
 * Pre-conditions: N/A
 * Post-conditions: output set for either terminal output or file output with
 *              file successfully opened for appending.
 ***********************************************/
void prompt_output_method(Output& output);


/**************************************************
 * Name: execute_action()
 * Description: This function will execute the provided action.
 * Parameters:  action_choice - action to execute
 *              playlists - array of playlist data
 *              num_playlists - number of playlist structs in playlists
 *              age_restricted - whether to display age restricted songs
 *              output - output stream to write to
 * Pre-conditions: each playlist in playlists has been initialized, output is
 *              set for either terminal output or file appending.
 * Post-conditions: specified action has been executed.
 ***********************************************/
void execute_action(const Action action_choice, const Playlist* playlists,
                    const int num_playlists, const bool age_restricted,
                    Output& output);


/**************************************************
 * Name: run_action_loop()
 * Description: This function will continually prompt the user for an action
 *              to execute until the user chooses to quit. If action isn't
 *              OUTPUT_ALL_SONGS or QUIT, the user will be prompted whether to
 *              output to the screen or to append to a file. If action is
 *              OUTPUT_ALL_SONGS, output will be set to the screen. If action
 *              is QUIT, program execution will terminate.
 * Parameters:  playlists - array of playlist data
 *              num_playlists - number of playlist structs in playlists
 *              age_restricted - whether to display age restricted songs
 * Pre-conditions: each playlist in playlists has been initialized.
 * Post-conditions: N/A
 ***********************************************/
void run_action_loop(Playlist* playlists, const int num_playlists,
                     const bool age_restricted);


/**************************************************
 * Name: main()
 * Description: Initializes playlist array with data from input file provided
 *              by user. Enters a loop that prompts the user for an action to execute
 *              until the user chooses to quit
 * Parameters:  N/A
 * Pre-conditions: N/A
 * Post-conditions: All allocated memory has been freed.
 ***********************************************/
int main();


#endif

