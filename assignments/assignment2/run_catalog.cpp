#include <iostream>
#include <fstream>

#include "catalog.h"
#include "input.h"
#include "output.h"
#include "run_catalog.h"

#define NUM_ACTIONS 6
#define NUM_OUTPUT_OPTIONS 2

using namespace std;


void open_input_file(ifstream& input_file) {
    const string input_file_name {read_string("Enter the name of an input "
                                              "playlist file: ")};

    input_file.open(input_file_name, ios::in);

    if (!input_file.is_open() || input_file.fail()) {
        cerr << "Error opening input file\n";
        exit(EXIT_FAILURE);
    }
}


void prompt_output_method(Output& output) {
    static const string output_options[] {
        "Output to screen",
        "Output to file"
    };

    const int selection {read_menu_selection(output_options,
                                             NUM_OUTPUT_OPTIONS)};
    const OutputOption output_option {static_cast<OutputOption>(selection)}; 
    switch (output_option) {
        case OutputOption::SCREEN:
            break;
        case OutputOption::FILE:
            const string output_file_name {read_string("Enter the name of the "
                                                       "output file: ")};
            output.open_file(output_file_name);
            break;
    }
}


/*
 * Over 15 lines. Alternatives include function pointers or inherited objects
 * but I think a simple switch statement looks cleaner and is more readable.
 */
void execute_action(const Action action_choice, const Playlist* playlists,
                    const int num_playlists, const bool age_restricted,
                    Output& output) {
    switch (action_choice) {
        case Action::OUTPUT_ALL_SONGS:
            output_all_songs(playlists, num_playlists, age_restricted, output);
            break;
        case Action::SEARCH_BY_TITLE:
            search_by_title(playlists, num_playlists, age_restricted, output);
            break;
        case Action::SEARCH_BY_GENRE:
            search_by_genre(playlists, num_playlists, age_restricted, output);
            break;
        case Action::OUTPUT_PLAYLIST_DURATIONS:
            output_playlist_durations(playlists, num_playlists, output);
            break;
        case Action::SORT_BY_YEAR:
            sort_playlist_songs_by_year(playlists, num_playlists);
            output_all_songs(playlists, num_playlists, age_restricted, output);
            break;
        case Action::QUIT:
            cout << "\nBye!\n\n";
            break;
    }
}


void run_action_loop(Playlist* playlists, const int num_playlists,
                     const bool age_restricted) {

    static const string action_menu[] {
        "Display all songs",
        "Search by title",
        "Search by genre",
        "Display playlist durations",
        "Sort songs by year",
        "Quit"
    };

    Action action_choice {};
    do {

        const int selection {read_menu_selection(action_menu, NUM_ACTIONS)};
        action_choice = static_cast<Action>(selection);

        Output output;
        if (action_choice != Action::OUTPUT_ALL_SONGS
         && action_choice != Action::QUIT) {
            prompt_output_method(output);
        }  // else: defaults to cout

        output_thick_separator(output);
        execute_action(action_choice, playlists, num_playlists, age_restricted,
                       output);
        output_thick_separator(output);

    } while (action_choice != Action::QUIT);
}


int main() {

    cout << "Welcome to Tune Finder!\n\n";

    ifstream playlist_file {};
    open_input_file(playlist_file);

    Playlist* playlists {};
    int num_playlists {};
    populate_playlists(playlists, num_playlists, playlist_file);
    playlist_file.close();

    const int user_age {read_positive_int("Enter your age: ")};
    const bool age_restricted {user_age <= 19};

    run_action_loop(playlists, num_playlists, age_restricted);

    delete_info(playlists, num_playlists);
    return EXIT_SUCCESS;
}

