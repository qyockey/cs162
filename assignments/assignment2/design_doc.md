# Assignment 2 Design Doc

Quinn Yockey \
934 519 469 \
CS 162-014 \
February 4, 2024

## Understanding the Problem

- Prompt user for playlist file. Don't repeat until valid path entered, instead
    print error and halt if path is invalid. *Why? Wouldn't it be better to
    repeat until a valid path is entered?*
- Prompt for user age
- Prompt for whether output should be written to screen or a file. If file is
    selected, prompt for file name. If file exists, written data should be
    appended
- Prompt for action: display all songs, play/search for a song, search songs
    by genre, get duration of all playlists, or quit. Repeat this until user
    selects quit.
  - Display all songs: If user age is greater than or equal to 20, iterate over
      all songs in all playlists: print title, artist, year of release,
      duration, genre, and restriction
  - Play/search for song: Prompt for song title. If song is found, output song
      info. No age check is required. If song is not found, alert user that
      the playlists do not contain their desired song.
  - Search songs by genre: Prompt user for genre, then output info of all songs
      in the selected genre. Age check not needed.
  - Get duration of all playlists: Output total duration of each playlist. Age
      check not needed.
  - Sort by year: Print each playlist with songs sorted by year
  - Quit: Halt program execution

## Proposed Implementation

### My Member Variable Name Modifications

```cpp
// a struct to hold info of a Playlist
struct Playlist {
    string name;           // name of the playlist, one word
    int num_songs;         // number of songs in the playlist
    struct Song *songs;    // an array that holds all songs in the playlist
    float total_duration;  // total duration of the playlist
};

// a struct to hold info of a Song
struct Song {
    string title;       // title of the song, one word
    string artist;      // name of the artist, one word
    int year_release;   // the year of release
    float duration;     // duration of the song
    string genre;       // genre of the song, one word
    string restriction; // "E" or "none" for restriction level (should
                        //      instead be bool or enum)
};
```

### Pseudocode

```text

create_playlists:
    Allocate memory for given number of playlists
    return pointer to allocated memory


populate_one_list:
    set playlist to playlist at playlist_index in playlist_array
    Read playlist_file into playlist's name
    Read playlist_file into playlist's num_songs
    set playlist's songs to result of calling create_songs
    set playlist's total_duration to 0
    for each song in playlist
        call populate_one_song
        add song duration to playlist duration


create_songs:
    Allocate memory for given number of songs
    return pointer to allocated memory


populate_one_song:
    set song to song at song_index of song_array
    Read playlist_file into song's title
    Read playlist_file into song's artist
    Read playlist_file into song's year_release
    Read playlist_file into song's duration
    Read playlist_file into song's genre
    Read playlist_file into song's restriction


delete_info:
    for each playlist
        Deallocate all memory allocated to playlist's songs
    Deallocate all memory allocated to playlist_array


print_song:
    Write song's title to output_file
    Write song's artist to output_file
    Write song's year_release to output_file
    Write song's duration to output_file
    Write song's genre to output_file
    Write song's restriction to output_file


display_all_songs:
    for each playlist
        set playlist to playlist at playlist_index of playlist_array
        for each song in playlist
            set song to song at song_index of song_array
            if song's restriction == "E" and not age_restricted:
                print_song


search_for_title:
    Prompt user for search_title

    // Allows for multiple songs with the same title
    set match_found to false

    for each playlist
        set playlist to playlist at playlist_index of playlist_array
        for each song in playlist
            set song to song at song_index of song_array
            if song's title == search_title:
                print_song
                set match_found to true

    if not match_found:
        Alert user no song matched their search


search_for_genre:
    Prompt user for search_genre
    set match_found to false
    for each playlist:
        set playlist to playlist at playlist_index of playlist_array
        for each song in playlist:
            set song to song at song_index of song_array
            if song's genre == search_genre:
                print_song
                set match_found to true

    if not match_found:
        Alert user no genres matched their search


print_playlist_durations:
    for each playlist:
        set playlist to playlist at playlist_index of playlist_array
        Write "{playlist_name}: {playlist.total_duration}" to output_file


quicksort:  // O(n log n)
    // if one or zero elements, already sorted
    if left >= right: return

    // pick arbitrary partition element
    set partition to (left + right) / 2
    
    // move partition to temporary postition at array[left]
    swap partition with left

    // index of last sorted element less than or equal to partition
    set subset_boundary to left

    // separate rest of array into smaller and larger subsets
    for each element from left + 1 to right:
        if (element <= partition):
            increment subset_boundary
            swap element with subset_boundary
    
    // restore partiton at subset boundary
    swap left and subset_boundary

    // left to subset_boundary - 1 now contains elements <= partition
    // subset_boundary + 1 to right now contains elements > partition

    // sort each subset
    quicksort array from left to subset_boundary - 1
    quicksort array from subset_boundary + 1 to right


print_playlists_sorted_by_year:
    for each playlist:
        create songs_sorted on heap with same size as songs
        copy songs to songs_sorted
        call quicksort on songs_sorted
        for each song in songs_sorted:
            print_song


main:
    Prompt user for playlist file, open as playlist_file
    if error opening playlist_file:
        alert user of error
        halt execution

    Read first line into num_playlists
    call create_playlists
    for each playlist from index 0 to num_playlists - 1:
        call populate_one_list
    close playlist_file

    Prompt user for age
    set bool age_restricted to if age <= 19

    Prompt user for desired output method (print to screen or write to file)
    if user wants to write to file:
        do
            Prompt user for output file path
        while path isn't valid, doesn't have write permissions,
                or is same as playlist_file
        open file path as output_file with app flag set
    else:
        open stdout as output_file  // I know this works in C, unsure about C++

    do
        Prompt for action
        if action != QUIT:
            call corresponding action function
    while action != QUIT

    delete_info

```

## Testing

### playlist_file_name

| Input                  | Expected Behavior                       | Meets Expectations |
|------------------------|-----------------------------------------|--------------------|
| "songs_playlist.txt"   | opens input stream songs_playlist.txt   | yes                |
| "another_playlist.txt" | opens input stream another_playlist.txt | yes                |
| "no_extension"         | opens input stream no_extension         | yes                |
| "doesnt_exist.txt"     | prints error and exits                  | yes                |
| "unsupported.format"   | prints error and exits                  | yes                |
| ""                     | prints error and exits                  | yes                |

### age

| Input             | Expected Behavior             | Meets Expectations |
|-------------------|-------------------------------|--------------------|
| "9"               | sets age_restriction to true  | yes                |
| "19"              | sets age_restriction to true  | yes                |
| "20"              | sets age_restriction to false | yes                |
| "99"              | sets age_restriction to false | yes                |
| "{INT_MAX}"       | sets age_restriction to false | yes                |
| "{INT_MAX + 1}"   | prints error and exits        | yes                |
| "0"               | prints error and exits        | yes                |
| "-1"              | prints error and exits        | yes                |
| "I'm old enough!" | prints error and exits        | yes                |
| ""                | prints error and exits        | yes                |

### screen_or_file_output

| Input         | Expected Behavior           | Meets Expectations |
|---------------|-----------------------------|--------------------|
| "1"           | opens stdout as output_file | yes                |
| "2"           | prompts for output_file     | yes                |
| "3"           | prints error and reprompts  | yes                |
| "0"           | prints error and reprompts  | yes                |
| "-1"          | prints error and reprompts  | yes                |
| ""            | prints error and reprompts  | yes                |
| "some string" | prints error and reprompts  | yes                |

### output_file_name

| Input                | Expected Behavior                                                    | Meets Expectations |
|----------------------|----------------------------------------------------------------------|--------------------|
| "doesnt_exist.txt"   | opens output stream doesnt_exist.txt for writing                     | yes                |
| "already_exists.txt" | opens output stream already_exists.txt for writing with ate flag set | yes                |
| "playlist_file.txt"  | prints error, reprompts                                              | yes                |
| "no_write_perms.txt" | prints error, reprompts                                              | yes                |
| ""                   | prints error, reprompts                                              | yes                |

### action

| Input         | Expected Behavior                               | Meets Expectations |
|---------------|-------------------------------------------------|--------------------|
| "1"           | displays all age-appropriate songs              | yes                |
| "2"           | search for song                                 | yes                |
| "3"           | search for genre                                | yes                |
| "4"           | prints duration of all playlists                | yes                |
| "5"           | frees all heap memory, halts program exectution | yes                |
| "6"           | prints error and reprompts                      | yes                |
| "0"           | prints error and reprompts                      | yes                |
| "-1"          | prints error and reprompts                      | yes                |
| ""            | prints error and reprompts                      | yes                |
| "some string" | prints error and reprompts                      | yes                |

### search_title

| Input                   | Expected Behavior                                      | Meets Expectations |
|-------------------------|--------------------------------------------------------|--------------------|
| "Nothing_Else_Matters"  | prints all songs matching title "Nothing_Else_Matters" | yes                |
| "Contains Spaces"       | prints error and reprompts                             | yes                |
| ""                      | prints error and reprompts                             | yes                |

### search_genre

| Input             | Expected Behavior                      | Meets Expectations |
|-------------------|----------------------------------------|--------------------|
| "Rock"            | prints all songs matching genre "Rock" | yes                |
| "Contains Spaces" | prints error and reprompts             | yes                |
| ""                | prints error and reprompts             | yes                |
