#include <chrono>
#include <iostream>
#include <fstream>
#include <string>

#include "catalog.h"
#include "input.h"
#include "output.h"

using namespace std;


Playlist* create_playlists(int num_playlists) {

    if (num_playlists <= 0) {
        cerr << "Error: number of playlists must be positive. " << num_playlists
             << " is invalid.\n";
        exit(EXIT_FAILURE);
    }

    Playlist* playlists {new Playlist[num_playlists]};

    if (!playlists) {
        cerr << "Error: unable to allocate memory for playlists.\n";
        exit(EXIT_FAILURE);
    }

    return playlists;

}


void populate_one_list(Playlist* playlists, int playlist_index,
                       ifstream& playlist_file) {
    Playlist& playlist {playlists[playlist_index]};

    playlist_file >> playlist.name >> playlist.num_songs;

    if (playlist.num_songs <= 0) {
        cerr << "Error: number of songs in playlist " << playlist.name
             << " must be positive.\n";
        exit(EXIT_FAILURE);
    }

    playlist.songs = create_songs(playlist.num_songs);

    for (int song_index {0}; song_index < playlist.num_songs; ++song_index) {
        populate_one_song(playlist.songs, song_index, playlist_file);
        playlist.total_duration += playlist.songs[song_index].duration;
    }
}


Song* create_songs(int num_songs) {

    if (num_songs <= 0) {
        cerr << "Error: number of songs must be positive. " << num_songs
             << " is invalid.\n";
        exit(EXIT_FAILURE);
    }

    Song* songs {new Song[num_songs]};

    if (!songs) {
        cerr << "Error: unable to allocate memory for songs.\n";
        exit(EXIT_FAILURE);
    }

    return songs;
}


void populate_one_song(Song* song_array, int song_index,
                       ifstream& playlist_file) {
    Song& song {song_array[song_index]};

    playlist_file >> song.title
                  >> song.artist
                  >> song.year_release
                  >> song.duration
                  >> song.genre
                  >> song.restriction;
} 


void delete_info(Playlist* playlists, int num_playlists) {
    for (int playlist_index {0}; playlist_index < num_playlists;
         ++playlist_index) {
        Playlist& playlist {playlists[playlist_index]};
        delete[] playlist.songs;
        playlist.songs = nullptr;
    }
    delete[] playlists;
    playlists = nullptr;
}


void populate_playlists(Playlist*& playlists, int& num_playlists,
                        ifstream& playlist_file) {
    playlist_file >> num_playlists;
    playlists = create_playlists(num_playlists);
    for (int playlist_index {0}; playlist_index < num_playlists;
         ++playlist_index) {
        populate_one_list(playlists, playlist_index, playlist_file);
    }
}


void output_thin_separator(Output& output) {
    output << "----------------------------------------\n\n";
}


void output_thick_separator(Output& output) {
    output << "========================================\n\n";
}


void output_song(const Song& song, const bool age_restricted, Output& output) {
    const bool is_explicit {song.restriction == "E"};
    if (!(is_explicit && age_restricted)) {
        output << "   Title: " << song.title << "\n"
                  "  Artist: " << song.artist << "\n"
                  "Released: " << song.year_release << "\n"
                  "Duration: " << song.duration << "\n"
                  "   Genre: " << song.genre << "\n"
                  "Explicit: " << (is_explicit ? "Yes" : "No") << "\n"
                  "\n";
    }
}


void output_playlist(const Playlist& playlist, const bool age_restricted,
                     Output& output) {
    output << "Playlist: " << playlist.name << "\n\n";
    for (int song_index {0}; song_index < playlist.num_songs;
            ++song_index) {
        const Song& song {playlist.songs[song_index]};
        output_song(song, age_restricted, output);
    }
}


void output_all_songs(const Playlist* playlists, const int num_playlists,
                      const bool age_restricted, Output& output) {
    output << "All songs:\n\n";
    for (int playlist_index {0}; playlist_index < num_playlists;
         ++playlist_index) {
        const Playlist& playlist {playlists[playlist_index]};
        output_playlist(playlist, age_restricted, output);

        if (playlist_index != num_playlists - 1) {
            output_thin_separator(output);
        }
    }
}


void search_by_title(const Playlist *playlists, const int num_playlists,
                     const bool age_restricted, Output& output) {
    string search_title {read_string_no_spaces("Enter title to search for: ")};

    bool match_found {false};

    for (int playlist_index {0}; playlist_index < num_playlists;
         ++playlist_index) {
        const Playlist& playlist {playlists[playlist_index]};

        for (int song_index {0}; song_index < playlist.num_songs;
             ++song_index) {
            const Song& song {playlist.songs[song_index]};

            if (song.title == search_title) {
                match_found = true;
                output_song(song, age_restricted, output);
            }
        }
    }

    if (!match_found) {
        output << "No songs found with title \"" << search_title << "\"\n\n";
    }
}


void search_by_genre(const Playlist *playlists, const int num_playlists,
                     const bool age_restricted, Output& output) {
    string search_genre {read_string_no_spaces("Enter genre to search for: ")};

    bool match_found {false};

    for (int playlist_index {0}; playlist_index < num_playlists;
         ++playlist_index) {
        const Playlist& playlist {playlists[playlist_index]};

        for (int song_index {0}; song_index < playlist.num_songs;
             ++song_index) {
            const Song& song {playlist.songs[song_index]};

            if (song.genre == search_genre) {
                match_found = true;
                output_song(song, age_restricted, output);
            }
        }
    }

    if (!match_found) {
        output << "No songs found with genre \"" << search_genre << "\"\n\n";
    }
}


void output_playlist_durations(const Playlist* playlists,
                               const int num_playlists, Output& output) {
    output << "Playlist durations:\n\n";
    for (int playlist_index {0}; playlist_index < num_playlists;
         ++playlist_index) {
        const Playlist& playlist {playlists[playlist_index]};
        output << playlist.name << ": " << playlist.total_duration << '\n';
    }
    output << '\n';
}


void sort_playlist_songs_by_year(const Playlist* playlists,
                                 const int num_playlists) {
    for (int playlist_index {0}; playlist_index < num_playlists;
         ++playlist_index) {

        const Playlist& playlist {playlists[playlist_index]};
        quicksort_year(playlist.songs, 0, playlist.num_songs - 1);
    }
}


void swap_songs(Song *songs, const int a, const int b) {
    const Song temp {songs[a]};
    songs[a] = songs[b];
    songs[b] = temp;
}


void quicksort_year(Song* songs, const int start, const int end) {

    // exit if one or zero elements to sort, already sorted
    if (start >= end) {
        return;
    }

    // year to compare against, pick middle of array
    const int partition {(start + end) / 2};
    const int partition_year {songs[partition].year_release};

    // switch partition to start
    swap_songs(songs, start, partition);

    // values to left are less than partition, to the right are greater
    int partition_boundary {start};

    // move values to left of boundary if greater than partition
    for (int index {partition_boundary + 1}; index <= end; ++index) {
        if (songs[index].year_release > partition_year) {
            ++partition_boundary;
            swap_songs(songs, index, partition_boundary);
        }
    }

    // restore partiton at boundary
    swap_songs(songs, start, partition_boundary);

    // sort lower and upper subsets
    quicksort_year(songs, start, partition_boundary - 1);
    quicksort_year(songs, partition_boundary + 1, end);

}

