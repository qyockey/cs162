#include <string>

/*********************************************************************
 * Function: read_string
 * Description: Prompts the user and returns the string entered.
 *     If EOF is encountered, clears cin errors and prompts again.
 *     This might be problematic if input was being read from a file
 *     or pipe, but this application is only intented to accept input
 *     from stdin.
 * Parameters:
 *     prompt - Prompt to display to the user.
 * Pre-Conditions:
 *     prompt is a valid string
 * Post-Conditions:
 *     return value is a valid string
 ********************************************************************/
std::string read_string(const std::string_view& prompt);


/*********************************************************************
 * Function: read_string_no_spaces
 * Description: Prompts the user and returns the string entered.
 *     If the provided string contains space characters, prompts
 *     again until a valid string is entered.
 * Parameters:
 *     prompt - Prompt to display to the user.
 * Pre-Conditions:
 *     prompt is a valid string
 * Post-Conditions:
 *     return value is a valid string
 ********************************************************************/
std::string read_string_no_spaces(const std::string_view& prompt);


/*********************************************************************
 * Function: read_positive_int
 * Description: Prompts the user for a positive integer. Repeats until
 *     a valid integer is entered, then returns the inputed integer.
 * Parameters:
 *     prompt - Prompt to display to the user.
 * Pre-Conditions:
 *     prompt is a valid string
 *     max is a positive integer
 * Post-Conditions:
 *     return value is a positive integer less than or equal to max
 ********************************************************************/
int read_positive_int(const std::string_view& prompt,
                      const int max = __INT_MAX__);


/*********************************************************************
 * Function: read_bool
 * Description: Prompts the user with a yes or no question. Repeats
 *     until the user enters y or n. Returns true if the user enters
 *     y, false if the user enters n.
 * Parameters:
 *     prompt - Prompt to display to the user.
 * Pre-Conditions:
 *     prompt is a valid string
 * Post-Conditions:
 *     return value is a boolean
 ********************************************************************/
bool read_bool(const std::string_view& prompt);


/*********************************************************************
 * Function: read_menu_selection()
 * Description: Prompts the user with a list of options and returns
 *     the index of the selected option. Repeats until a valid option
 *     is entered.
 * Parameters:
 *     menu - array of strings representing the menu options
 *     num_options - number of options in the menu
 * Pre-Conditions:
 *     menu is an initialized string array with size num_options
 * Post-Conditions:
 *     1 <= return value <= num_options
 ********************************************************************/
int read_menu_selection(const std::string* menu, const int num_options);

