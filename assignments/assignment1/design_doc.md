# Assignment 1 Design Doc

Quinn Yockey \
934 519 469 \
Section 014

## Understanding the Problem

- Prompt for number of players, must be int > 0
- For each player:
    - Prompt for money ball rack location. 1 <= input int <= 5
    - Prompt for shooting ability. 1 <= input int <= 99
    - Simulate five racks of shots
        - Players make shots with percentage equal to shooting ability
        - Print shots missed, shots made, and money ball shots made
        - Print rack score
    - Print total score
- Print player with highest score, or players with tie
- Prompt to play again

## Proposed Implementation

```text
seed random number genrator with current time
do
    do
        ask for number of players
    while n is int and > 0
    for each player
        do
            ask for for money ball rack location
        while location is int and 1 <= location <= 5
        do
            ask for shooting ability
        while ability is int and 1 <= ability <= 99
        for each of 5 racks
            declare arrary of shot states
            for each of 5 shots
                get random integer in range [0, 99]
                shot is made if random int < shooting ability
                set shot states at i to missed, made, or money ball made
            print shot states and rack score
        print total score
    print player(s) with highest score
    do
        ask if user wants to play again [y/n]
    while input isn't "y" or "n"
while user wants to play again
```

## Testing

| Value                           | Expected Behavior              | Meets Expectations |
|-------------------------------- | ------------------------------ | ------------------ |
| numPlayers = 0                  | Error message, prompt again    | Yes                |
| numPlayers = -1                 | Error message, prompt again    | Yes                |
| numPlayers = "some string"      | Error message, prompt again    | Yes                |
| numPlayers = ""                 | Error message, prompt again    | Yes                |
| numPlayers = 3.5                | Error message, prompt again    | Yes                |
| numPlayers = 1                  | Simulate 1 player              | Yes                |
| numPlayers = 5                  | Simulate 5 players             | Yes                |
| numPlayers = EOF                | Error message, halt executuion | Yes                |
|                                 |                                |                    |
| moneyRack = 0                   | Error message, prompt again    | Yes                |
| moneyRack = -1                  | Error message, prompt again    | Yes                |
| moneyRack = "some string"       | Error message, prompt again    | Yes                |
| moneyRack = ""                  | Error message, prompt again    | Yes                |
| moneyRack = 3.5                 | Error message, prompt again    | Yes                |
| moneyRack = 8                   | Error message, prompt again    | Yes                |
| moneyRack = 1                   | Money ball rack at location 1  | Yes                |
| moneyRack = 5                   | Money ball rack at location 5  | Yes                |
| moneyRack = EOF                 | Error message, halt executuion | Yes                |
|                                 |                                |                    |
| shootingAbility = 0             | Error message, prompt again    | Yes                |
| shootingAbility = -1            | Error message, prompt again    | Yes                |
| shootingAbility = "some string" | Error message, prompt again    | Yes                |
| shootingAbility = ""            | Error message, prompt again    | Yes                |
| shootingAbility = 3.5           | Error message, prompt again    | Yes                |
| shootingAbility = 100           | Error message, prompt again    | Yes                |
| shootingAbility = 256           | Error message, prompt again    | Yes                |
| shootingAbility = 1             | 1% change of making shots      | Yes                |
| shootingAbility = 50            | 50% change of making shots     | Yes                |
| shootingAbility = 99            | 99% change of making shots     | Yes                |
| shootingAbility = EOF           | Error message, halt executuion | Yes                |
|                                 |                                |                    |
| playAgain = 0                   | Error message, prompt again    | Yes                |
| playAgain = -1                  | Error message, prompt again    | Yes                |
| playAgain = 3.5                 | Error message, prompt again    | Yes                |
| playAgain = "length > 1"        | Error message, prompt again    | Yes                |
| playAgain = ""                  | Error message, prompt again    | Yes                |
| playAgain = "a"                 | Error message, prompt again    | Yes                |
| playAgain = "y"                 | Start from beginning           | Yes                |
| playAgain = "n"                 | Exit                           | Yes                |
| playAgain = EOF                 | Error message, halt executuion | Yes                |
