/*********************************************************************
** Program Filename: hooping.cpp
** Author: Quinn Yockey
** Date: 01/27/2024
** Description: This program simulates a basketball shooting game.
**     Each player gets 5 racks of 5 balls each, and the 5th ball in
**     each rack is a "money ball", worth 2 points instead of the
**     usual 1. Additionally, each player selects one of their
**     racks to be a "money ball rack" in which all 5 balls are money
**     balls. The player with the most points wins.
** Input:
**     - Number of players
**     - "Money ball rack" rack number for each player [1-5]
**     - Shooting ability for each player [1%-99%]
** Output:
**     - Visual for shots made per rack: 'X' for misses, 'O' for
**            normal buckets, and 'M' for money ball buckets
**     - Points per rack
**     - Points per player
**     - Winner(s)
*********************************************************************/

#include <cstdlib>
#include <ctime>
#include <iostream>
#include <random>
#include <string>

#define NUM_RACKS 5
#define BALLS_PER_RACK 5
#define MONEY_BALL_INDEX 5

enum class SHOT_STATE { MISS, NORMAL_BUCKET, MONEY_BUCKET };

std::string readString(const std::string_view& prompt);
int readPositiveInt(const std::string_view& prompt,
                    const int max = __INT_MAX__);
bool readBool(const std::string_view& prompt);
void simulateGame(const int numPlayers);
int simulatePlayer(const int playerNum);
int simulateRack(SHOT_STATE rackShots[BALLS_PER_RACK],
                 const int shootingAbility, const bool isMoneyBallRack);
void printRack(const int rackNum, const SHOT_STATE rackShots[BALLS_PER_RACK],
               const int rackScore);
SHOT_STATE simulateShot(const int shootingAbility, const bool isMoneyBall);
void printWinners(const int numTiedPlayers, const int *tiedPlayers);

/*********************************************************************
** Function: main
** Description: Prompts user for number of players and runs the
**     shooting game. Repeats as long user wants to keep playing.
** Parameters: void
** Pre-Conditions: N/A
** Post-Conditions: 
**     - All allocated memory is freed
**     - All exceptions and errors are caught and handled
*********************************************************************/
int main() {
    srand(static_cast<unsigned>(time(nullptr)));
    std::cout << "\nWelcome to the shooting contest!\n";
    do {
        const int numPlayers {readPositiveInt("How many players? ")};
        simulateGame(numPlayers);
    } while (readBool("\nDo you want to play again? [y/n] "));
    return EXIT_SUCCESS;
}

/*********************************************************************
** Function: simulateGame
** Description: Runs a single iteration of the shooting game for each
**     player and prints the winner(s).
** Parameters:
**     - numPlayers: Number of players to simulate
** Pre-Conditions:
**     - numPlayers > 0
** Post-Conditions:
**     - All allocated memory is freed
**     - maxScore in range [0, 34]
**     - numTiedPlayers in range [1, numPlayers]
**     - First numTiedPlayers elements of tiedPlayers are in
**           range [1, numPlayers]
*********************************************************************/
void simulateGame(const int numPlayers) {
    int *tiedPlayers = new int[numPlayers];
    tiedPlayers[0] = -1;
    int maxScore {-1};
    int numTiedPlayers {1};
    for (int playerNum {1}; playerNum <= numPlayers; ++playerNum) {
        const int playerScore {simulatePlayer(playerNum)};
        if (playerScore > maxScore) {
            maxScore = playerScore;
            numTiedPlayers = 1;
            tiedPlayers[0] = playerNum;
        } else if (playerScore == maxScore) {
            tiedPlayers[numTiedPlayers] = playerNum;
            ++numTiedPlayers;
        }
    }
    if (numPlayers > 1) {
        printWinners(numTiedPlayers, tiedPlayers);
    }
    delete[] tiedPlayers;
}

/*********************************************************************
** Function: printWinners
** Description: Prints the winner(s) of the shooting game.
** Parameters:
**     - numTiedPlayers: Number of players with the same high score
**     - tiedPlayers: Array of player numbers with the same high score
** Pre-Conditions:
**     - numTiedPlayers > 0
**     - First numTiedPlayers elements of tiedPlayers are in
**           range [1, numPlayers]
** Post-Conditions:
**     - tiedPlayers is unchanged
*********************************************************************/
void printWinners(const int numTiedPlayers, const int *tiedPlayers) {
    if (numTiedPlayers == 1) {
        std::cout << "Player " << tiedPlayers[0] << " wins!\n";
    } else {
        std::cout << numTiedPlayers << "-way tie between players ";
        for (int tiedPlayerNum {0}; tiedPlayerNum < numTiedPlayers;
             ++tiedPlayerNum) {
            std::cout << tiedPlayers[tiedPlayerNum];
            if (tiedPlayerNum < numTiedPlayers - 1 && numTiedPlayers > 2) {
                std::cout << ',';
            }
            if (tiedPlayerNum != numTiedPlayers - 1) {
                std::cout << ' ';
            }
            if (tiedPlayerNum == numTiedPlayers - 2) {
                std::cout << "and ";
            }
        }
        std::cout << "!\n";
    }
}

/*********************************************************************
** Function: simulatePlayer
** Description: Prompts the user for the location of the money ball
**     rack and for the sooting ability of the player. Simulates the
**     shooting game for the player and returns the player's score.
** Parameters:
**     - playerNum: Number of current player
** Pre-Conditions:
**     - playerNum in range [1, numPlayers]
** Post-Conditions:
**     - moneyBallRack in range [1, 5]
**     - shootingAbility in range [1, 99]
**     - playerScore in range [0, 34]
*********************************************************************/
int simulatePlayer(const int playerNum) {
    std::cout << "\nPlayer " << playerNum << ":\n";
    const int moneyBallRack {
            readPositiveInt("Money ball rack location? [1-5] ", 5)};
    const int shootingAbility {
            readPositiveInt("Shooting ability? [1-99] ", 99)};
    int playerScore {0};
    std::cout << '\n';
    for (int rackNum {1}; rackNum <= NUM_RACKS; ++rackNum) {
        SHOT_STATE rackShots[BALLS_PER_RACK];
        const bool isMoneyBallRack {rackNum == moneyBallRack};
        const int rackScore {
                simulateRack(rackShots, shootingAbility, isMoneyBallRack)};
        playerScore += rackScore;
        printRack(rackNum, rackShots, rackScore);
    }
    std::cout << "\nTotal: " << playerScore << " pts\n";
    return playerScore;
}

/*********************************************************************
** Function: simulateRack
** Description: Simulates a single rack of the shooting game. Returns
**     the score for the rack.
** Parameters:
**     - shootingAbility: Shooting ability of the player
**     - isMoneyBallRack: Whether the current rack is a money ball rack
** Pre-Conditions:
**     - rackShots is an array of size BALLS_PER_RACK
**     - shootingAbility in range [1, 99]
** Post-Conditions:
**     - Each element of rackShots is in enum SHOT_STATE
**     - rackScore in range [0, 10]
*********************************************************************/
int simulateRack(SHOT_STATE rackShots[BALLS_PER_RACK],
                 const int shootingAbility, const bool isMoneyBallRack) {
    int rackScore {0};
    for (int ballNum {1}; ballNum <= BALLS_PER_RACK; ++ballNum) {
        bool isMoneyBall {isMoneyBallRack || ballNum == MONEY_BALL_INDEX};
        SHOT_STATE shotState {simulateShot(shootingAbility, isMoneyBall)};
        rackShots[ballNum - 1] = shotState;
        switch (shotState) {
            case SHOT_STATE::MISS:
                break;
            case SHOT_STATE::NORMAL_BUCKET:
                rackScore += 1;
                break;
            case SHOT_STATE::MONEY_BUCKET:
                rackScore += 2;
                break;
        }
    }
    return rackScore;
}

/*********************************************************************
** Function: printRack
** Description: Prints the results of a single rack of the shooting
**     game. Prints rack number, 'X' for misses, 'O' for normal
**     buckets, and 'M' for money buckets, and finally the total
**     score for the rack.
** Parameters:
**     - rackNum: Number of the current rack
**     - rackShots: Array of shot states for the current rack
**     - rackScore: Score for the current rack
** Pre-Conditions:
**     - rackNum in range [1, NUM_RACKS]
**     - rackShots is an initialized array of length BALLS_PER_RACK, and
**           each element is in enum SHOT_STATE
**     - rackScore in range [0, 10]
** Post-Conditions:
**     - rackShots is unchanged
*********************************************************************/
void printRack(const int rackNum, const SHOT_STATE rackShots[BALLS_PER_RACK],
               const int rackScore) {
    std::cout << "Rack " << rackNum << ": ";
    for (int ballNum {1}; ballNum <= BALLS_PER_RACK; ++ballNum) {
        switch (rackShots[ballNum - 1]) {
            case SHOT_STATE::MISS:
                std::cout << "X ";
                break;
            case SHOT_STATE::NORMAL_BUCKET:
                std::cout << "O ";
                break;
            case SHOT_STATE::MONEY_BUCKET:
                std::cout << "M ";
                break;
        }
    }
    std::cout << "| " << rackScore << " pts\n";
}

/*********************************************************************
** Function: simulateShot
** Description: Simulates a single shot. Returns the result of the
**     shot. A shot is considered made if a random integer between 0
**     and 99 is less than the shooting ability. rand() has a slight
**     bias towards 0 through 47 over 48 through 99, but this is
**     negligible for this assignment.
** Parameters:
**     - shootingAbility: Percentage of shots player is expected
**           to make
**     - isMoneyBall: Whether the current shot is a money ball
** Pre-Conditions:
**     - rand() is seeded
**     - shootingAbility in range [1, 99]
** Post-Conditions:
**     - return value in enum SHOT_STATE
*********************************************************************/
SHOT_STATE simulateShot(const int shootingAbility, const bool isMoneyBall) {
    if (rand() % 100 < shootingAbility) {
        if (isMoneyBall) {
            return SHOT_STATE::MONEY_BUCKET;
        }
        return SHOT_STATE::NORMAL_BUCKET;
    }
    return SHOT_STATE::MISS;
}

/*********************************************************************
** Function: readString
** Description: Prompts the user and returns the string entered.
**     If EOF is encountered, clears cin errors and prompts again.
**     This might be problematic if input was being read from a file
**     or pipe, but this application is only intented to accept input
**     from stdin.
** Parameters:
**     - prompt: Prompt to display to the user.
** Pre-Conditions:
**     - prompt is a valid string
** Post-Conditions:
**     - return value is a valid string
*********************************************************************/
std::string readString(const std::string_view& prompt) {
    std::string input;
    while (true) {
        std::cout << prompt;
        std::getline(std::cin >> std::ws, input);
        if (std::cin.eof()) {
            std::cout << "EOF encountered. Continuing.\n";
            clearerr(stdin);
            std::cin.clear();
        } else {
            break;
        }
    }
    return input;
}

/*********************************************************************
** Function: readPositiveInt
** Description: Prompts the user for a positive integer. Repeats until
**     a valid integer is entered, then returns the inputed integer.
** Parameters:
**     - prompt: Prompt to display to the user.
** Pre-Conditions:
**     - prompt is a valid string
**     - max is a positive integer
** Post-Conditions:
**     - return value is a positive integer less than or equal to max
*********************************************************************/
int readPositiveInt(const std::string_view& prompt, const int max) {
    while (true) {
        std::string input {readString(prompt)};
        try {
            const int value {std::stoi(input)};
            if (std::to_string(value) != input) {
                throw std::invalid_argument {""};
            }
            if (1 <= value && value <= max) {
                return value;
            }
            throw std::out_of_range {""};
        } catch (std::invalid_argument&) {
            std::cout << "Value must be a positive integer. Try again.\n";
        } catch (std::out_of_range&) {
            std::cout << "Value is out of range. Enter a value from 1 to "
                      << max << ".\n";
        }
    }
}

/*********************************************************************
** Function: readBool
** Description: Prompts the user with a yes or no question. Repeats
**     until the user enters y or n. Returns true if the user enters
**     y, false if the user enters n.
** Parameters:
**     - prompt: Prompt to display to the user.
** Pre-Conditions:
**     - prompt is a valid string
** Post-Conditions:
**     - return value is a boolean
*********************************************************************/
bool readBool(const std::string_view& prompt) {
    while (true) {
        if (const std::string input {readString(prompt)}; input.length() == 1) {
            const char firstChar {input.at(0)};
            if (firstChar == 'y') {
                return true;
            } else if (firstChar == 'n') {
                return false;
            }
        }
        std::cout << "Value must be 'y' or 'n'. Try again.\n";
    }
}
