#include <array>
#include <cstring>
#include <ncurses.h>
#include <sstream>

#include "game.h"
#include "gold.h"
#include "input.h"
#include "pool.h"
#include "rope.h"
#include "room.h"
#include "stalactites.h"
#include "wumpus.h"

#define STARTING_ARROWS             3
#define MAX_ARROW_DISTANCE          3

#define WINDOW_TOP_LET              0, 0
#define ARROW_WIN_DIMENSIONS        1, 9
#define ARROW_WIN_TOP_LEFT          0, 0
#define OXYGEN_WIN_DIMENSIONS       1, 11
#define OXYGEN_WIN_TOP_LEFT         1, 0
#define BOARD_WIN_TOP_LEFT          3, 0
#define PERCEPT_WIN_DIMENSIONS      4, 40
#define INSTRUCTION_WIN_DIMENSIONS  6, 40

#define BOARD_TOP_LEFT_CELL         Coords(1, 2)
#define BOARD_SCALAR                Coords(2, 4)

#define OFFSET_UP                   Coords(-1, 0)
#define OFFSET_LEFT                 Coords(0, -1)
#define OFFSET_DOWN                 Coords(1, 0)
#define OFFSET_RIGHT                Coords(0, 1)
#define OFFSET_NULL                 Coords(0, 0)


Game::Game(const int side, const bool debug) noexcept :
        side_length(side),
        debug_view(debug),
        rooms(side_length, std::vector<Room>(side_length, Room())),
        start_location(get_random_empty_room_location()),
        game_over(false),
        adventurer(this->start_location, 2 * side) {

    get_room_at_coords(start_location).set_event(new Rope);

    // randomly insert events (2 pool accesses, 2 stalactites, 1 wumpus, 1 gold)
    get_random_empty_room().set_event(new Gold);
    get_random_empty_room().set_event(new Wumpus);
    for (int i {0}; i < 2; ++i) {
        get_random_empty_room().set_event(new Pool);
        get_random_empty_room().set_event(new Stalactites);
    }

    arrows_display = newwin(ARROW_WIN_DIMENSIONS, ARROW_WIN_TOP_LEFT);
    oxygen_display = newwin(OXYGEN_WIN_DIMENSIONS, OXYGEN_WIN_TOP_LEFT);
    const Coords board_size {Coords(side_length, side_length) * BOARD_SCALAR
                             + Coords(1, 1)};
    board = newwin(get_coords(board_size), BOARD_WIN_TOP_LEFT);
    percepts = newwin(PERCEPT_WIN_DIMENSIONS, 5 + board_size.row, 0);
    instructions = newwin(INSTRUCTION_WIN_DIMENSIONS, 10 + board_size.row, 0);
    refresh();
}


Game::~Game() noexcept {
    delwin(arrows_display);
    delwin(oxygen_display);
    delwin(board);
    delwin(percepts);
    delwin(instructions);
}


Coords Game::get_random_empty_room_location() const noexcept {
    int row;
    int col;
    const Room* p_room;
    do {
        row = rand() % side_length;
        col = rand() % side_length;
        p_room = &get_room_at_coords({row, col});
    } while (!p_room->is_empty());

    return {row, col};
}


Room& Game::get_room_at_coords(const Coords coords) const noexcept {
    const int row {coords.row};
    const int col {coords.col};

    // make room not const
    try {
        return const_cast<Room&>(rooms.at(row).at(col));
    } catch (std::out_of_range&) {

    }
}


Room& Game::get_random_empty_room() const noexcept {
    return get_room_at_coords(get_random_empty_room_location());
}


Coords Game::get_move_offsets(const int move_direction) const noexcept {
    switch (move_direction) {
        case 'w': return OFFSET_UP;
        case 'a': return OFFSET_LEFT;
        case 's': return OFFSET_DOWN;
        case 'd': return OFFSET_RIGHT;
        default:  return OFFSET_NULL;
    }
}


int Game::get_arrow_direction() const noexcept {
    mvwprintw(instructions, WINDOW_TOP_LET,
             "Fire an arrow....\n"
             "W-up\n"
             "A-left\n"
             "S-down\n"
             "D-right\n"
             "Enter direction: ");
    refresh();
    wrefresh(instructions);

    int direction;
    while (true) {
        direction = getch();
        switch (direction) {
            case 'w':
            case 'a':
            case 's':
            case 'd':
                return direction;
            default:
                break;
        }
    }
}


bool Game::out_of_bounds(const Coords& coords) const noexcept {
    return coords.row < 0 || coords.row >= side_length
        || coords.col < 0 || coords.col >= side_length;
}


bool Game::at_rope() const noexcept {
    return adventurer.get_location() == start_location;
}


void Game::wumpus_move() noexcept {
    // after a missed arrow, 75% chance that the wumpus is moved to a different room

    // How to get 75%? 
    // Hint: generate a random number from 0-3, if the number is not 0, then move

    // Your code here:
    mvwprintw(instructions, WINDOW_TOP_LET,
             "Game::wumpus_move() is not implemented...\n");
    refresh();
    wrefresh(instructions);
    
    return;
}


void Game::fire_arrow() noexcept {
    const int direction {get_arrow_direction()};
    const Coords movement {get_move_offsets(direction)};

    Coords arrow_room_coords {adventurer.get_location() + movement};
    for (int arrow_distance {0}; arrow_distance < MAX_ARROW_DISTANCE;
         ++arrow_distance) {
        if (out_of_bounds(arrow_room_coords)) {
            break;
        }
        Room& arrow_room = get_room_at_coords(arrow_room_coords);
        if (arrow_room.get_event_type() == Event::EventType::WUMPUS) {
            arrow_room.destroy_contents();
            if (rand() % 4 == 0) {
                Game::pause_with_message("You hit the wumpus!");
            } else {
                Game::pause_with_message("You woke up the wumpus!");
                Game::get_random_empty_room().set_event(new Wumpus);
            }
            break;
        }

        arrow_room_coords += movement;
    }
    adventurer.decrement_arrows();

    return;
}


void Game::move_player(const int direction) noexcept {
    const Coords offsets {Game::get_move_offsets(direction)};
    const Coords old_location {adventurer.get_location()};
    const Coords new_location {old_location + offsets};

    if (Game::out_of_bounds(new_location)) {
        return;
    }

    adventurer.move(offsets);
    if (adventurer.is_underwater()) {
        adventurer.decrement_oxygen();
        if (adventurer.get_oxygen() == 0) {
            adventurer.die();
            return;
        }
    }
    Room& new_room {get_room_at_coords(adventurer.get_location())};
    new_room.encounter(adventurer);
    if (!adventurer.is_underwater()
     && new_room.get_event_type() == Event::EventType::GOLD) {
        new_room.destroy_contents();
    }

    print_percepts();
}


void Game::action(const int action_input) noexcept {
    if (action_input == 'f') {
        Game::fire_arrow();
        Game::update_arrows_display();
    } else {
        Game::move_player(action_input);
        if (!adventurer.is_alive() || (adventurer.has_gold() && at_rope())) {
            this->game_over = true;
        }
    }
    return;
}


void Game::print_move_instructions() const noexcept {
    mvwprintw(instructions, WINDOW_TOP_LET,
             "Player move...\n\n"
             "W-up\n"
             "A-left\n"
             "S-down\n"
             "D-right\n"
             "F-fire an arrow\n");
    refresh();
    wrefresh(instructions);
}


void Game::pause_with_message(const std::string message) const noexcept {
    werase(instructions);
    wrefresh(instructions);
    mvwprintw(instructions, WINDOW_TOP_LET, "%s\nPress any key to continue.\n",
              message.c_str());
    refresh();
    wrefresh(instructions);
    getch();
}


int Game::get_input() const noexcept {
    Game::print_move_instructions();
    while (true) {
        const int input {getch()};
        switch (input) {
            case 'w':
            case 'a':
            case 's':
            case 'd':
                return input;
            case 'f':
                if (adventurer.is_underwater()) {
                    Game::pause_with_message("Cannot fire arrow while "
                                             "underwater.");
                    Game::print_move_instructions();
                    break;
                } else if (adventurer.get_num_arrows() <= 0) {
                    Game::pause_with_message("Out of arrows.");
                    Game::print_move_instructions();
                    break;
                }
                return input;
            default:
                break;
        }
    }

}


void Game::play_game() noexcept {

    Game::display_game();
    Game::print_percepts();

    while (!game_over) {
        // Player move...
        // 1. get input
        const int input {Game::get_input()};

        // 2. move player
        Game::action(input);

        // 3. may or may not encounter events
        // Your code here:
        display_game();
    }

    werase(percepts);
    wrefresh(percepts);
    Game::pause_with_message(adventurer.is_alive() ?
                             "You escaped!" : "Game Over");
    return;

}


std::string Game::get_row_separator() const noexcept {
    static std::string row_separator{};
    if (!row_separator.empty()) {
        return row_separator;
    }

    std::stringstream separator_stream;
    for (int col {0}; col < side_length; ++col) {
        separator_stream << "+---";
    }
    separator_stream << "+";

    row_separator = separator_stream.str();
    return row_separator;
}


char Game::get_room_symbol(const int row, const int col) const noexcept {
    // room containing player
    if (adventurer.get_location() == Coords(row, col)) {
        return '*';
    }

    // show room events if in debug mode
    if (const Room& room {get_room_at_coords({row, col})};
        !room.is_empty() && debug_view) {
        return room.get_symbol();
    }

    // otherwise all rooms displayer empty
    return ' ';
}


void Game::display_game() const noexcept {
    static const std::string row_separator {get_row_separator()};

    update_arrows_display();
    update_oxygen_display();

    wmove(board, WINDOW_TOP_LET);
    wprintw(board, "%s", row_separator.c_str());
    for (int row {0}; row < side_length; ++row) {
        for (int col {0}; col < side_length; ++col) {
            wprintw(board, "| %c ", get_room_symbol(row, col));
        }
        wprintw(board, "|%s", row_separator.c_str());
    }
    refresh();
    wrefresh(board);

    // example output (when finished): 
    //  +---+---+---+---+
    //  | P | G | P |   |
    //  +---+---+---+---+
    //  |   | W |   | S |
    //  +---+---+---+---+   
    //  |   |   |   | S |
    //  +---+---+---+---+   
    //  | * |   |   |   |
    //  +---+---+---+---+
}


void Game::print_percepts() const noexcept {
    const static std::array<Coords, 4> cardinal_offsets {
        OFFSET_UP, OFFSET_LEFT, OFFSET_DOWN, OFFSET_RIGHT
    };

    wmove(percepts, WINDOW_TOP_LET);

    int num_empty_adjacent_rooms {0};

    for (auto const& offset : cardinal_offsets) {
        const Coords adjacent_coords {adventurer.get_location() + offset};
        if (out_of_bounds(adjacent_coords)) {
            continue;
        }

        const Room& adjecent {Game::get_room_at_coords(adjacent_coords)};
        if (!adjecent.is_empty()) {
            wprintw(percepts, "%s", adjecent.get_percept().c_str());
        } else {
            ++num_empty_adjacent_rooms;
        }
    }

    for (int i {0}; i < num_empty_adjacent_rooms; ++i) {
        wprintw(percepts, "\n");
    }

    refresh();
    wrefresh(percepts);
}


void Game::update_arrows_display() const noexcept {
    mvwprintw(arrows_display, WINDOW_TOP_LET, "Arrows: %d",
              adventurer.get_num_arrows());
    refresh();
    wrefresh(arrows_display);
}


void Game::update_oxygen_display() const noexcept {
    mvwprintw(oxygen_display, WINDOW_TOP_LET, "Oxygen: %-3d",
              adventurer.get_oxygen());
    refresh();
    wrefresh(oxygen_display);
}


Coords Game::room_coords_to_screen_coords(const Coords& coords)
        const noexcept {
    return coords * BOARD_SCALAR + BOARD_TOP_LEFT_CELL;
}


// void Game::update_player_icon(const Coords& old_coords,
//                               const Coords& new_coords) const noexcept {
//     const char old_room_symbol {debug_view
//                                 ? get_room_at_coords(old_coords).get_symbol()
//                                 : ' '};
//     mvwprintw(board, get_coords(room_coords_to_screen_coords(old_coords)),
//              "%c", old_room_symbol);
//     mvwprintw(board, get_coords(room_coords_to_screen_coords(new_coords)), "*");
//     refresh();
//     wrefresh(board);
// }

