#pragma once

#include "coords.h"

class Adventurer {
private:
    Coords location;
    bool alive;
    bool gold_collected;
    bool underwater;
    int max_oxygen;
    int oxygen;
    int num_arrows;

public:
    Adventurer(const Coords& start_coords, const int max_oxygen) noexcept;

    const Coords& get_location() const noexcept;
    bool is_alive() const noexcept;
    bool has_gold() const noexcept;
    bool is_underwater() const noexcept;
    int get_oxygen() const noexcept;
    int get_num_arrows() const noexcept;

    void move(const Coords& offsets) noexcept;
    void die() noexcept;
    void collect_gold() noexcept;
    void toggle_underwater() noexcept;
    void decrement_oxygen() noexcept;
    void reset_oxygen() noexcept;
    void decrement_arrows() noexcept;
};

