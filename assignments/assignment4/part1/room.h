#pragma once

#include <optional>

#include "adventurer.h"
#include "event.h"

class Room {

private: 
    std::optional<Event*> event;

public:
    Room() noexcept;
    Room(Event* event) noexcept;
    ~Room() noexcept;

    bool is_empty() const noexcept;
    void destroy_contents() noexcept;
    char get_symbol() const noexcept;
    Event::EventType get_event_type() const noexcept;
    std::string get_percept() const noexcept;
    void set_event(Event* event) noexcept;
    void encounter(Adventurer& adventurer) noexcept;
};

