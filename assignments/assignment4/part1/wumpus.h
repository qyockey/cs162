#pragma once

#include "adventurer.h"
#include "event.h"
#include "game.h"

class Wumpus : public Event {
public:
    Wumpus();
    void encounter(Adventurer& adventurer) noexcept;
};


