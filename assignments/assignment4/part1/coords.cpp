#include "coords.h"

void Coords::threshold_value(int& value, const int min, const int max)
        const noexcept {
    if (value < min) {
        value = min;
    } else if (value > max) {
        value = max;
    }
}

Coords::Coords() noexcept = default;

Coords::Coords(const int row, const int col) noexcept
    : row(row), col(col) {
}

// Coords& Coords::operator=(const Coords& other) noexcept = default;

Coords Coords::operator-() const noexcept {
    return { -row, -col };
}

Coords Coords::operator+(const Coords& other) const noexcept {
    return { this->row + other.row,
                this->col + other.col };
}

Coords Coords::operator+(const std::initializer_list<int>& other)
        const noexcept {
    auto it = other.begin();
    return { this->row + *it++,
             this->col + *it };
}

Coords Coords::operator-(const Coords& other) const noexcept {
    return *this + (-other);
}

Coords Coords::operator*(const Coords& other) const noexcept {
    return { this->row * other.row,
                this->col * other.col };
}

Coords& Coords::operator+=(const Coords& other) noexcept {
    this->row += other.row;
    this->col += other.col;
    return *this;
}

Coords& Coords::operator-=(const Coords& other) noexcept {
    return *this += (-other);
}

bool Coords::operator==(const Coords& other) const noexcept {
    return this->row == other.row && this->col == other.col;
}

bool Coords::operator==(const Coords&& other) const noexcept {
    return this->row == std::move(other.row)
        && this->col == std::move(other.col);
}

bool Coords::operator==(const std::initializer_list<int>& other)
        const noexcept {
    if (other.size() != 2) {
        return false;
    }

    auto it = other.begin();
    return row == *it++ && col == *it;
}

bool Coords::operator!=(const Coords& other) const noexcept {
    return !(*this == other);
}

void Coords::threshold(const int min, const int max) noexcept {
    threshold_value(this->row, min, max);
    threshold_value(this->col, min, max);
}

