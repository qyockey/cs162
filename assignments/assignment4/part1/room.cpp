#include "room.h"

Room::Room() noexcept {
}


Room::Room(Event* e) noexcept {
    Room::set_event(e);
}


Room::~Room() noexcept {
    Room::destroy_contents();
}


bool Room::is_empty() const noexcept {
    return !event.has_value();
}


void Room::destroy_contents() noexcept {
    if (!Room::is_empty()) {
        delete event.value();
    }
    event.reset();
}


char Room::get_symbol() const noexcept {
    return Room::is_empty() ? ' ' : event.value()->get_symbol();
}


std::string Room::get_percept() const noexcept {
    return Room::is_empty() ? "" : event.value()->get_percept();
}


Event::EventType Room::get_event_type() const noexcept {
    return Room::is_empty()
         ? Event::EventType::EMPTY
         : event.value()->get_type();
}


void Room::set_event(Event* e) noexcept {
    Room::destroy_contents();
    event.emplace(e);
}

void Room::encounter(Adventurer& adventurer) noexcept {
    if (!Room::is_empty()) {
        event.value()->encounter(adventurer);
    }
}

