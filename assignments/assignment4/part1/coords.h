#pragma once

#include <utility>

#define get_coords(coords) coords.row, coords.col

struct Coords {
private:
    void threshold_value(int& value, const int min, const int max)
            const noexcept;

public:
    int row;
    int col;

    Coords() noexcept;
    Coords(const int row, const int col) noexcept;

    // Coords& operator=(const Coords& other) noexcept;
    Coords operator-() const noexcept;
    Coords operator+(const Coords& other) const noexcept;
    Coords operator+(const std::initializer_list<int>& other) const noexcept;
    Coords operator-(const Coords& other) const noexcept;
    Coords operator*(const Coords& other) const noexcept;
    Coords& operator+=(const Coords& other) noexcept;
    Coords& operator-=(const Coords& other) noexcept;
    bool operator==(const Coords& other) const noexcept;
    bool operator==(const Coords&& other) const noexcept;
    bool operator==(const std::initializer_list<int>& other) const noexcept;
    bool operator!=(const Coords& other) const noexcept;

    void threshold(const int min, const int max) noexcept;

};

