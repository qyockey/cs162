#pragma once

#include "adventurer.h"
#include "event.h"
#include "game.h"

class Stalactites : public Event {
public:
    Stalactites();
    void encounter(Adventurer& adventurer) noexcept;
};

