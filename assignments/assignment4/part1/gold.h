#pragma once

#include "adventurer.h"
#include "event.h"
#include "game.h"

class Gold : public Event {
public:
    Gold() noexcept;
    void encounter(Adventurer& adventurer) noexcept;
};

