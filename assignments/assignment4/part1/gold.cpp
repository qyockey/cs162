#include "gold.h"
#include "event.h"


Gold::Gold() noexcept :
    Event::Event('G', "You see a shimmer nearby\n", EventType::GOLD) {
}


void Gold::encounter(Adventurer& adventurer) noexcept {
    if (!adventurer.is_underwater()) {
        adventurer.collect_gold();
    }
}

