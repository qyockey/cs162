#include "adventurer.h"

#define STARTING_ARROWS 3

Adventurer::Adventurer(const Coords& start_coords, const int max_oxygen)
    noexcept :
        location(start_coords),
        alive(true),
        gold_collected(false),
        underwater(false),
        max_oxygen(max_oxygen),
        oxygen(max_oxygen),
        num_arrows(STARTING_ARROWS) {
}


const Coords& Adventurer::get_location() const noexcept { return location; }


bool Adventurer::is_alive() const noexcept { return alive; }


bool Adventurer::has_gold() const noexcept { return gold_collected; }


bool Adventurer::is_underwater() const noexcept { return underwater; }


int Adventurer::get_oxygen() const noexcept { return oxygen; }


int Adventurer::get_num_arrows() const noexcept { return num_arrows; }


void Adventurer::move(const Coords& offsets) noexcept { location += offsets; }


void Adventurer::die() noexcept { alive = false; }


void Adventurer::collect_gold() noexcept { gold_collected = true; }


void Adventurer::toggle_underwater() noexcept { underwater = !underwater; }


void Adventurer::decrement_oxygen() noexcept { --oxygen; }


void Adventurer::reset_oxygen() noexcept { oxygen = max_oxygen; }


void Adventurer::decrement_arrows() noexcept { --num_arrows; }


