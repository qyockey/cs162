#pragma once

#include "adventurer.h"
#include "event.h"
#include "game.h"

class Pool : public Event {
public:
    Pool();
    void encounter(Adventurer& adventurer) noexcept;
};


