#pragma once

#include "adventurer.h"
#include "event.h"
#include "game.h"

class Rope : public Event {
public:
    Rope();
    void encounter(Adventurer& adventurer) noexcept;
};


