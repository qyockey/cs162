#include "game.h"
#include "pool.h"

#include <iostream>


Pool::Pool() : Event('P', "You hear wind blowing\n", EventType::POOL) {
}

void Pool::encounter(Adventurer& adventurer) noexcept {
    adventurer.toggle_underwater();
    if (!adventurer.is_underwater()) {
        adventurer.reset_oxygen();
    }
}

