#include "event.h"
#include "game.h"
#include "stalactites.h"

#include <iostream>


Stalactites::Stalactites()
    : Event::Event('S', "You hear water dripping\n", EventType::STALACTITES) {
}


void Stalactites::encounter(Adventurer& adventurer) noexcept {
    if (rand() % 2 == 0 && !adventurer.is_underwater()) {
        adventurer.die();
    }
}
