#include "event.h"
#include "wumpus.h"

#include <iostream>


Wumpus::Wumpus() :
    Event::Event('W', "You smell a terrible stench\n", EventType::WUMPUS) {
}


void Wumpus::encounter(Adventurer& adventurer) noexcept {
    if (!adventurer.is_underwater()) {
        adventurer.die();
    }
}

