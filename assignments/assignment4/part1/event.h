#pragma once

#include <string>
#include <cstdint>

#include "adventurer.h"

class Event {

public:
    enum class EventType { EMPTY, GOLD, ROPE, POOL, STALACTITES, WUMPUS };

    Event(const char symbol, const char* percept, const EventType type)
            noexcept;
    virtual ~Event() noexcept;

    char get_symbol() const noexcept;
    EventType get_type() const noexcept;
    std::string get_percept() const noexcept;
    virtual void encounter(Adventurer& adventurer) noexcept = 0;

protected:
    const char debug_symbol;
    const char* percept_message;
    const EventType event_type;

};

