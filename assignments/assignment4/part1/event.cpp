#include "event.h"

#include <iostream>

Event::Event(const char symbol, const char* percept, const EventType type)
        noexcept :
    debug_symbol(symbol),
    percept_message(percept),
    event_type(type) {
}


Event::~Event() noexcept = default;


char Event::get_symbol() const noexcept { return debug_symbol; }


std::string Event::get_percept() const noexcept { return percept_message; }


Event::EventType Event::get_type() const noexcept { return event_type; }

