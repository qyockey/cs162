#include <ctime>
#include <ncurses.h>
#include <term.h>

#include "game.h"
#include "input.h"

#define CAVE_SIDE_MIN 4
#define CAVE_SIDE_MAX 50


int main() {
    // set the random seed
    srand(time(nullptr));

    // const int side_len {read_positive_int("Enter cave side length: ",
    //                                       CAVE_SIDE_MIN, CAVE_SIDE_MAX)};
    // const bool debug_view {read_bool("Would you like to play in debug mode? "
    //                                  "[y/n] ")};
    WINDOW* screen {initscr()};
    noecho();
    curs_set(false);

    // Game g(side_len, debug_view);
    Game g(5, true);
    g.play_game();

    delwin(screen);
    endwin();
    exit_terminfo(0);
}

