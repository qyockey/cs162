#pragma once

#include <iostream> 
#include <ncurses.h>
#include <vector>

#include "adventurer.h"
#include "coords.h"
#include "room.h"

class Game {
private:
    
    const int side_length;        // side of the board
    const bool debug_view;        // debug mode or not
    std::vector<std::vector<Room> > rooms;
    const Coords start_location;
    bool game_over;
    Adventurer adventurer;

    WINDOW* arrows_display;
    WINDOW* oxygen_display;
    WINDOW* board;
    WINDOW* percepts;
    WINDOW* instructions;

public:

    Game(const int side, const bool debug) noexcept;
    ~Game() noexcept;
    
    Coords get_random_empty_room_location() const noexcept;
    Room& get_room_at_coords(const Coords coords) const noexcept;
    Room& get_random_empty_room() const noexcept;
    Coords get_move_offsets(const int move_direction) const noexcept;
    int get_arrow_direction() const noexcept;
    bool out_of_bounds(const Coords& coords) const noexcept;
    bool at_rope() const noexcept;
    void wumpus_move() noexcept;
    void fire_arrow() noexcept;
    void move_player(const int move_direction) noexcept;
    void action(const int) noexcept;
    void print_move_instructions() const noexcept;
    void pause_with_message(const std::string message) const noexcept;
    int get_input() const noexcept;
    void play_game() noexcept;

    std::string get_row_separator() const noexcept;
    char get_room_symbol(const int row, const int col) const noexcept;
    void display_game() const noexcept;
    void print_percepts() const noexcept;
    void update_arrows_display() const noexcept;
    void update_oxygen_display() const noexcept;
    Coords room_coords_to_screen_coords(const Coords& coords) const noexcept;
    void update_player_icon(const Coords& old_coords,
                            const Coords& new_coords) const noexcept;
};

