# Assignment 4 Design Doc

Quinn Yockey  
CS 162  
March 10th, 2024  

## Hunt the Wumpus

### Understanding the Problem

- Prompt user for dimensions of square cave system between 4 and 50 inclusive
- Prompt for whether or not to play in debug mode
- Create 2d grid of given size using vectors
- Place wumpus in one random room, 2 stalactites in empty random rooms, 2 pools in empty random rooms, and 1 gold in a random empty room
- Place player in a random empty room and assign escape rope to player room
- Repeat until player escapes with gold or dies:
    - Prompt for direction to move or to fire arrow
        - If firing an arrow, arrow travels for up to three rooms or until it hits a wal. If arrow goes into room containing wumpus, there is a 25% chance it will kill the wumpus and a 75% chance it misses the wumpus, causing the wumpus to move to a random empty room.
        - If moving, ensure the player isn't moving into a wall. As they enter the new room, display their location within the map and print all precepts for the adjecent rooms
            - If moving into a room with a pool, prompt the user for whether they want to dive and explore underwater. If they coose to dive, ignore collisions and possible fatal encounters until they surface at either of the two pools or if they spend a number of turns underwater more than two times the cave system side length
            - If entering room with stalactites, 50% chance of death and 50% change of survivial
            - If entering room with the wumpus, player is killed immediately
            - If entering room with gold, the gold is picked up and the room becomes empty
            - If entering room with escape rope without gold, print that the adventurer must find the gold before they can leave. If the adventurer has the gold, they escape and win the game

### Proposed Implementation

#### Event

```cpp
class Event:

private variables:
    char debug_symbol
    string percept_message

public functions:
    print_symbol:
        print debug_symbol

    print_percept:
        print percept_message

    pure virtual void event_action(game)
```

#### Gold

```cpp
class Gold is-a Event:
private variables:
    char debug_symbol = $
    string percept_message = "You see a glimmer nearby"

public_functions:
    void event_action(game):
        set has_gold in game to true
        set event of room containing gold to empty
```

#### Pool

```cpp
class Pool is-a Event:
private variables:
    char debug_symbol = P
    string percept_message = "You hear wind blowing"

public_functions:
    void event_action(game):
        ask player if they want to dive underwater
        if yes:
            set underwater to true
```

#### Stalactite

```cpp
class Stalactite is-a Event:
private variables:
    char debug_symbol = S
    string percept_message = "You hear water dripping"

public_functions:
    void event_action(game):
        if random integer from 0 to 1 is 0:
            set player_alive to false
```

#### Wumpus

```cpp
class Wumpus is-a Event:
private variables:
    char debug_symbol = W
    string percept_message = "You smell a terrible stench"

public_functions:
    void event_action(game):
        set player_alive to false
```

#### Rope

```cpp
class Rope is-a Event:
private variables:
    char debug_symbol = ~
    string percept_message = ""

public_functions:
    void event_action(game):
    if has_gold is false:
        print message you must grab the gold before leaving
```

#### Empty

```cpp
class Empty is-a Event:
private variables:
    char debug_symbol = ' '
    string percept_message = ""

public_functions:
    void event_action(game):
        return
```

#### Room

```cpp
class Room:
private variables:
    Event event

public functions:
    print_event_symbol:
        event.print_symbol
    event_action:
        event.execute_action
```

#### Game

```cpp
class Game:

private variables:
    2d vector of rooms
    int side_length
    pair<int> player_location
    bool player_alive
    bool underwater
    int oxygen_level
    int num_arrows
    bool debug_mode

public functions:
    void init:
        ask user for side_length from 4 to 50
        initialize rooms to side_length x side_length array of empty rooms
        ask user for debug_mode true or false
        set player_alive to true
        set oxygen_level to 2 * side_length
        set num_arrows to 3
        set event of get_random_empty_room to gold
        set event of get_random_empty_room to wumpus
        repeat twice:
            set event of get_random_empty_room to stalactites
            set event of get_random_empty_room to pool

    void run:
        init
        while player_alive is true:
            ask user for action
            wait until user enters w, a, s, d, or f
            if action is f:
                fire_arrow
            else:
                move_direction = get_move_direction(action)
                move_player(move_direction)

    void fire_arrow:
        if num_arrows is 0:
            print message out of arrows
            return
        ask user which direction to fire arrow
        wait until user enters w, a, s, or d
        pair<int> arrow_direction = get_move_direction(action)
        decrement num_arrows
        for arrow_distance from 1 to 3
            set arrow_room to player_location
                    + arrow_distance * arrow_direction
            if out_of_bounds(arrow_room)
                break
            if room at arrow_room contains wumpus
                set room.event to empty
                if random int from 0 to 3 is 0:
                    print message wumpus has been killed
                else
                    set event of get_random_empty_room to wumpus

    void move_player(pair<int> move_direction):
        new_location = player_location + move_direction
        if out_of_bounds(new_location):
            print essage cannot move into wall
            return
        player_location = new_location
        player_room = game at player_location
        print_cave()
        if not underwater:
            player_room.event_action()
        else
            decrement oxygen_level
            if oxygen_level is 0:
                set player_alive to false
                print message death from drowning
        if not player_alive:
            return
        for each room adjecent to player_room
            if not out_of_bounds(room):
                room.print_percept();

    Room get_random_empty_room:
        do 
            row = random integer between 0 and side_length - 1
            col = random integer between 0 and side_length - 1
            room = game at row, col 
        while event at room is not empty:
        return room
            
    bool out_of_bounds(pair<int> location):
        return location.row < 0 or location.row > side_length
            or location.col < 0 or location.col > side_length:

    pair<int> get_move_direction(char action):
        // {d_row, d_col}
        switch action:
            case w: return {-1, 0}
            case a: return {0, -1}
            case s: return {1, 0}
            case d: return {0, 1}

    void print_cave:
        for row from 0 to side_length - 1:
            print_row_separator
            for col from 0 to side_length - 1:
                set room to rooms at row,col
                print '|'
                if player_location equals row,col:
                    print '*'
                else if debug_mode:
                    room.print_event_symbol
                else:
                    print ' '
            print '|'
            print newline
        print_row_separator

    void print_row_separator:
        repeat side_length times:
            print "+-"
        print '+'
        print newline
```

### Testing

#### action

| Input         | Expected Behavior           | Meets Expectations |
|---------------|-----------------------------|--------------------|
| w             | Move up    unless OOB, execute event action, print precepts of adjecent rooms | yes                |
| a             | Move left  unless OOB, execute event action, print precepts of adjecent rooms | yes                |
| s             | Move down  unless OOB, execute event action, print precepts of adjecent rooms | yes                |
| d             | Move right unless OOB, execute event action, print precepts of adjecent rooms | yes                |
| f             | Read direction, shoot arrow | yes                |
| ""            | Print error, reprompt       | yes                |
| "some string" | Print error, reprompt       | yes                |

## Recursive Stairs

### Understanding the Problem

- Prompt user for number of stairs in staircase
- Output number of ways to get to the top of the staircase if steps of 1 stair, 2 stairs, and three stairs are allowed

### Proposed Implementation

```cpp
int ways_to_top(num_stairs):
    create integer step_combinations initialized to 0
    for step_size from 1 to 3
        if num_stairs is at least step_size:
            add ways_to_top(num_stairs - step_size) to step_combinations
        else
            break
    return step_combinations

```

### Testing

| Input        | Expected Behavior            | Meets Expectations |
|--------------|------------------------------|--------------------|
| 3            | Outputs 4                    | yes                |
| 4            | Outputs 7                    | yes                |
| 5            | Outputs 13                   | yes                |
| 6            | Outputs 24                   | yes                |
| 73           | Outputs 12903063846126135669 | yes                |
| 74           | Print error (too large for `uint64_t`), reprompt        | yes                |
| 0            | Print error, reprompt        | yes                |
| -1           | Print error, reprompt        | yes                |
| ""           | Print error, reprompt        | yes                |
| "not an int" | Print error, reprompt        | yes                |

