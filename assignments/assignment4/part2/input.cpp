/*********************************************************************
** Program Filename: input.cpp
** Author: Quinn Yockey
** Date: 02/11/2024
** Description: This file contains various functions to read prompted
**              input from the user.
*********************************************************************/

#include <iostream>

using namespace std;

string read_string(const string_view prompt) {
    string input;
    while (true) {
        cout << prompt;
        getline(cin >> ws, input);

        if (cin.eof()) {
            // Input contains EOF character
            cout << "EOF character encountered, try again.\n";
            clearerr(stdin);
            cin.clear();

        } else {
            // valid input
            return input;
        }
    }
}


string read_string_no_spaces(const string_view prompt) {
    string input;
    while (true) {
        input = read_string(prompt);

        // if user is tricky and enters "\ "
        if (input.find(' ') == string::npos) {
            return input;
        }
        cout << "Input cannot contain spaces, try again.\n";
    }
}


int read_positive_int(const string_view prompt, const int max) {
    while (true) {
        string input {read_string(prompt)};

        try {
            const int value {stoi(input)};
            if (to_string(value) != input) {
                throw invalid_argument {""};
            }
            if (1 <= value && value <= max) {
                return value;
            }
            throw out_of_range {""};

        } catch (invalid_argument&) {
            cout << "Value must be a positive integer. Try again.\n";

        } catch (out_of_range&) {
            cout << "Value is out of range. Enter a value from 1 to " << max
                 << ".\n";
        }
    }
}


bool read_bool(const string_view prompt) {
    while (true) {
        if (const string input {read_string(prompt)}; input.length() == 1) {

            if (const char first_char {input.at(0)}; first_char == 'y') {
                return true;
            } else if (first_char == 'n') {
                return false;
            }
        }
        cout << "Value must be 'y' or 'n'. Try again.\n";
    }
}


int read_menu_selection(const string* menu, const int num_options,
                        const string_view prompt) {
    cout << '\n' << prompt << '\n';

    for (int option {0}; option < num_options; ++option) {
        const string_view menu_item {menu[option]};
        cout << option + 1 << ". " << menu_item << '\n';
    }

    const int selection {read_positive_int("Enter selection: ", num_options)};
    cout << '\n';
    return selection - 1;
}

