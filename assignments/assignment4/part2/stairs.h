#pragma once

#include <cstdint>
#include <vector>

class Stairs {

private:
    // vector to store results of computation for a given number of steps
    static std::vector<uint64_t> step_counts;

public:
    /**
     * function: ways_to_top(const int)
     * description: computes the number of ways to get to the
     *      top of a staircase with n steps using small (size 1),
     *      medium (size 2), and large (size 3) steps
     * parameters:
     *      int n: the number of steps in the staircase
     *  returns: the number of ways to get to the top. see the
     *     examples in the assignment description for more info.
     */
    static uint64_t ways_to_top(const int n);
};

