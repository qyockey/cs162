// The following is an example program that
// does some basic, limited testing of your
// ways_to_top() implementation.

#include <iostream>

#include "input.h"
#include "stairs.h"

#define MAX_STAIRS 73  // largest value that doesn't result in uint64_t overflow

int main() {
    bool run_again;
    do {
        const int num_steps {read_positive_int("Enter number of steps: ",
											   MAX_STAIRS)};
        std::cout << Stairs::ways_to_top(num_steps) << " possible combinations "
                     "of steps\n";
        run_again = read_bool("Would you like to test another number of "
                              "steps? [y/n]");
    } while (run_again);
    std::cout << "Bye!\n";
}
