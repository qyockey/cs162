#include <climits>

#include "stairs.h"

#define UNKNOWN LONG_LONG_MAX
#define MIN_STEP 1
#define MAX_STEP 3


std::vector<uint64_t> Stairs::step_counts;


uint64_t Stairs::ways_to_top(const int num_steps) {
    // increase size of computation storage vector if needed
    if (static_cast<int>(step_counts.size()) < num_steps + 1) {
        step_counts.resize(num_steps + 1, UNKNOWN);
        step_counts[0] = 1;
    }

    // return result if already computed
    if (step_counts.at(num_steps) != UNKNOWN) {
        return step_counts.at(num_steps);
    }

    // otherwise, run computation
    uint64_t combinations {0};
    for (int step_size {MIN_STEP}; step_size <= MAX_STEP; ++step_size) {
        if (num_steps >= step_size) {
            combinations += ways_to_top(num_steps - step_size);
        } else {
            break;
        }
    }

    // store computation for given number of steps
    step_counts[num_steps] = combinations;
    return combinations;
}

