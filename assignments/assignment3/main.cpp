/******************************************************************************* 
** Program: flight_manager
** Author: Quinn Yockey
** Date: 2/26/2024 
** Description: Simulates an air traffic control system in which flights are
**              moved between airports by user commands
** Input: User selects various actions to perform:
**        View all Airports & Flights info - print information of all airoprts
**                                           and flights
**        Check flight info - search for a flight by its flight number and print
**                            its flight info if found
**        Add a new flight - prompt user for info of new flight to add, add it
**                           to selected airport
**        Cancel a flight - user selects a flight from a chosen airport to
**                          remove
**        Take off a flight - user selects a flight; flight is moved from its
**                            its current location to its destination
**        Print airport stats - print information about all airports
**        Take off all flights - move all flights from their current locations
**                               to their destinations simultaneously
**        Quit - halt program execution
** Output:
**        Printed information is written to terminal screen
**        As flight and airport information is changed and flights are moved
**                 between airports, this information is updated in the
**                 airports text file
**        When taking off all flights simultaneously, the movement of flights is
**                 written to a log file
*******************************************************************************/

#include "manager.h"

int main() {
    Manager manager;
    manager.run();
    return EXIT_SUCCESS;
}

