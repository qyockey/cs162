# Assignment 2 Design Doc

Quinn Yockey  
934 519 469  
CS 162-014  
February 4, 2024

## Understanding the Problem

- Prompt user for action:
	- View all Airport and Flight info:
		- print info of each flight in each airport
	- Check flight info:
		- prompt user for flight number to search for
		- if flight not found: print error message
		- otherwise: print flight info
	- Add new flight:
		- prompt user for airport to add to
		- if airport at capacity: print error message
		- otherwise: prompt user for flight info and add flight to airport
	- Cancel a flight:
		- prompt user for flight number to cancel
		- if not found: print error message
		- otherwise: remove flight from its airport
	- Take off a flight:
		- prompt user for flight number to cancel
		- if not found: print error message
		- if destination airport at capacity: print error message
		- otherwise: remove flight from its current airport, add it to destination airport
	- Print stats:
		- print name, capacity, and number of flights with it as a destination
	- Quit:
		- halt program execution

## Proposed Implementation

note: getters, setters, other trivial functions omitted

```text

Flight::populate_flight:
	read flight number, current airport, destination, number of pilots,
			and pilots array from airport.txt


Flight::print_flight:
	write flight number, current airport, destination, number of pilots,
			and pilots array to terminal screen


Flight::set_incoming_flight:
	search array of airport references for airport matching destinatation
	pass flight reference to destination_airport.add_incoming_flight
	increment destination_airport.num_incoming_flights


/*************************************************************************/


Airport::populate_airport:
	read airport name, number of flights, and capacity from airport.txt
	allocate memory for flights
	for each flight:
		flight.populate_flight


Airport::print_airport:
	print airport name, capacity, and num_incoming_flights


Airport::print_flights:
	for each flight:
		flight.print_flight


Airport::flight_number_search:
	for each flight:
		if flight number equals provided flight number:
			return address of flight
	// no matches found
	return nullptr


Airport::at_capacity:
	return if number of flights equals capacity


Airport::add_flight:
	set flight array at index num_flights to provided flight
	increment num_flights


Airport::add_incoming_flight:
	set flight array at index num_incoming_flights to provided flight
	increment num_incoming_flights


Airport::remove_flight:
	if flight array doesn't contain flight with given flight number:
		print error message
		return
	delete flight from array
	shift remaining flights one index left
	// maybe convert array to binary tree instead for O(log n) removal


Airport::remove_incoming_flight:
	if incoming flight array doesn't contain flight with given flight number:
		print error message
		return
	delete flight from incoming flights array
	shift remaining flights one index left


Airport::generate_incoming_flights:
	for each flight:
		pass array of airport references to flight.set_incoming_flight


/*************************************************************************/


Mangaer::populate:
	read number of airports from airport.txt
	allocate memory for airports
	for each airport:
		airport.populate_airport


Manager::init:
	open airport.txt for reading
	this.populate
	
	for each airport:
		pass array of airport references to
				aiport.generate_incoming_flights


Manager::print_all:
	for each airport:
		airport.print_airport
		airport.print_flights


Manager::check_flight_control:
	read flight number from terminal input
	set match_found to false
	
	for each airport:
		set flight_match to result of calling airport.flight_number_search
				with provided flight number
		if flight_match is not nullptr:
			set match_found to true
			flight_match.print_flight
	
	if match_found is false:
		print flight not found error message


Manager::add_flight_control:
	read target airport from terminal input
	if airport.at_capacity
		print error message
		return
	
	allocate memory for new flight object
	// read current and destination airports as a menu selections to avoid
	// checking if they exist
	read flight number, current airport, destination, number of pilots,
			and pilots array from terminal input
	call airport.add_flight with reference flight


Manager::cancel_flight_control:
	read airport to remove flight from as menu option of all airports
	read flight to remove as menu option of all flights in selected airport
	pass flight to airport.remove


Manager::take_off_control:
	read airport to take off flight from as menu option of all airports
	read number of flight to take off as menu option of flights in
			selected airport
	search for current_airport and destination_airport in airport array
	
	if current_airport or destination_airport don't exist:
		print error
		return
	
	if destination_airport at capacity:
		print error
		return
	
	search current_airport for flight
	pass flight to current_airport.remove_flight
	pass flight to destination_airport.remove_incoming_flight
	pass flight to destination_airport.add_flight


Manager::stats_control:
	for each airport:
		airport.print_airport


Manager::run:
	this.init
	parse flight info from airport.txt
	populate each flight in each airport in manager
	
	do:
		prompt user for desired action
		execute functions to acheive action
	while action is not quit


/*************************************************************************/


main:
	create manager object
	manager.run

```

## Testing

### action

| Input | Expected Behavior | Meets Expectations |
| ---- | ---- | ---- |
| 1 | prints info for all airports and flights | yes |
| 2 | prompt for flight number; print flight info of matching flight | yes |
| 3 | prompt for flight info; add to desired airport | yes |
| 4 | prompt for flight number; remove from desired airport | yes |
| 5 | prompt for flight number; move from current location to destination | yes |
| 6 | print info for all airports | yes |
| 7 | gracefully halt execution | yes |
| 0 | print error, reprompt | yes |
| 8 | print error, reprompt | yes |
| "some string" | print error, reprompt | yes |
| "" | print error, reprompt | yes |

### flight

| Input | Expected Behavior | Meets Expectations |
| ---- | ---- | ---- |
| 1 | selects first flight in array | yes |
| 2 | selects second flight in array | yes |
| num_flights | selects last flight in array | yes |
| num_flights + 1 | print error, reprompt | yes |
| 0 | print error, reprompt | yes |
| "some string" | print error, reprompt | yes |
| "" | print error, reprompt | yes |

### airport

| Input | Expected Behavior | Meets Expectations |
| ---- | ---- | ---- |
| 1 | selects first airport in array | yes |
| 2 | selects second airport in array | yes |
| num_airports | selects last airport in array | yes |
| num_airports + 1 | print error, reprompt | yes |
| 0 | print error, reprompt | yes |
| "some string" | print error, reprompt | yes |
| "" | print error, reprompt | yes |

### flight number || pilot name

| Input | Expected Behavior | Meets Expectations |
| ---- | ---- | ---- |
| "any non-empty string" | sets flight number to input string | yes |
| "" | print error, reprompt | yes |

### num_pilots

| Input | Expected Behavior | Meets Expectations |
| ---- | ---- | ---- |
| 1 | sets num_pilots to 1 | yes |
| 2 | sets num_pilots to 2 | yes |
| INT_MAX | sets num_pilots to INT_MAX | yes |
| 0 | print error, reprompt | yes |
| INT_MAX + 1 | print error, reprompt | yes |
| "some string" | print error, reprompt | yes |
| "" | print error, reprompt | yes |
