#include "flight.h"

using namespace std;


Flight::Flight() = default;


Flight::Flight(const int new_num_pilots)
  : num_pilots(new_num_pilots),
    pilots(new string[new_num_pilots]) {
}


Flight::Flight(const Flight& other) {
    set_curr_loc(other.get_curr_loc());
    set_dest(other.get_dest());
    set_flight_num(other.get_flight_num());
    set_num_pilots(other.get_num_pilots());

    this->pilots = new string[num_pilots];
    for (int pilot_index {0}; pilot_index < num_pilots; ++pilot_index) {
        this->pilots[pilot_index] = other.get_pilots()[pilot_index];
    }
}
    

Flight& Flight::operator=(const Flight& other) {
    if (this == &other) {
        return *this;
    }

    if (this->pilots) {
        delete[] this->pilots;
        this->pilots = nullptr;
    }

    set_curr_loc(other.get_curr_loc());
    set_dest(other.get_dest());
    set_flight_num(other.get_flight_num());
    set_num_pilots(other.get_num_pilots());

    this->pilots = new string[num_pilots];
    for (int pilot_index {0}; pilot_index < num_pilots; ++pilot_index) {
        this->pilots[pilot_index] = other.get_pilots()[pilot_index];
    }

    return *this;
}


Flight::~Flight() {
    if (pilots) {
        delete[] pilots;
        pilots = nullptr;
    }
}


void Flight::populate_flight(ifstream& fin) {
    fin >> flight_num
        >> curr_loc
        >> dest
        >> num_pilots;
    
    pilots = new string[num_pilots];
    for (string* pilot = pilots; pilot < pilots + num_pilots; ++pilot) {
        fin >> *pilot;
    }

    return;
}


void Flight::write_to_file(ofstream& fout) const {
    fout << flight_num << ' '
         << curr_loc << ' '
         << dest << ' '
         << num_pilots << ' ';

    for (const string* pilot = pilots; pilot < pilots + num_pilots; ++pilot) {
        fout << *pilot;

        // print space separator unless last pilot
        if (pilot != pilots + num_pilots - 1) {
            fout << ' ';
        }
    }
    fout << '\n';
    
    return;
}


void Flight::set_flight_num(const string_view new_flight_num) {
    this->flight_num = new_flight_num;
    return;
}


void Flight::set_curr_loc(const string_view new_curr_loc) {
    this->curr_loc = new_curr_loc;
    return;
}


void Flight::set_dest(const string_view new_dest) {
    this->dest = new_dest;
    return;
}


void Flight::set_num_pilots(const int new_num_pilots) {
    this->num_pilots = new_num_pilots;
    return;
}


void Flight::set_pilots(string* new_pilots) {
    this->pilots = new_pilots;
    return;
}


string Flight::get_flight_num() const {
    return flight_num;
}


string Flight::get_curr_loc() const {
    return curr_loc;
}


string Flight::get_dest() const {
    return dest;
}


int Flight::get_num_pilots() const {
    return num_pilots;
}


string* Flight::get_pilots() const {
    return pilots;
}


void Flight::print_flight() const {
    cout << "\n"
            "   Flight number: " << flight_num << "\n"
            "Current location: " << curr_loc << "\n"
            "     Destination: " << dest << "\n"
            "Number of Pilots: " << num_pilots << "\n"
            "          Pilots: ";
    for (const string* pilot = pilots; pilot < pilots + num_pilots; ++pilot) {
        cout << *pilot << ' ';
    }
    cout << '\n';

    return;
}

