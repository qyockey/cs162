#ifndef AIRPORT_H
#define AIRPORT_H 

#include <iostream>
#include <string>
#include <fstream>

#include "flight.h"

class Airport {
private:
    std::string name= std::string();  // airport name
    int num_flights = 0;              // number of flights
    int capacity = -1;                // capacity
    Flight* flights = nullptr;        // flight array

public:
    Airport();
    Airport(const int capacity);
    Airport(const Airport& other);
    Airport& operator=(const Airport other);
    ~Airport();

    std::string get_name() const;
    int get_num_flights() const;
    int get_capacity() const;
    Flight* get_flights() const;
    Flight& get_flight(const int idx) const;
    bool at_capacity() const;

    void set_name(const std::string_view name);
    void set_num_flights(const int num_flights);
    void set_capacity(const int capacity);
    void set_flights(const Flight* flights, const int num_flights);

    void populate_airport(std::ifstream& fin);
    void write_to_file(std::ofstream& fout) const;
    int add_flight(const Flight& flight);
    Flight remove_a_flight(int idx);
    void print_airport(const int num_incoming_flights) const;
    void print_flights() const;
    Flight* flight_number_search(std::string_view flight_num) const;
    int get_num_flights_to_dest(const Airport& dest) const;
    int read_flight_index() const;
    Flight& read_input_flight() const;
    
};

#endif

