#ifndef QUEUE_H
#define QUEUE_H

template<typename T>
class Queue {

public:
    Queue() = default;


    ~Queue() {
        Node* current {head};
        while (current->next) {
            Node* next {current->next};
            delete[] current;
            current = next;
        }
        return;
    }


    void push(T* data) {
        if (!head || !tail) {
            head = new Node(data);
            tail = head;
            return;
        }

        Node* new_tail {new Node(data)};
        tail->next = new_tail;
        tail = new_tail;
        return;
    }


    T* pop() {
        if (!head) {
            return nullptr;
        }

        T* data {head->data};
        Node* new_head {head->next};
        delete[] head;
        head = new_head;

        return data;
    }


    bool empty() {
        return head == nullptr;
    }



private:
    using Node = struct Node {
        T* data;
        Node* next {nullptr};

        // Node() = default;
        Node(T* data) : data(data) {}
    };

    Node* head {nullptr};
    Node* tail {nullptr};

};

#endif
