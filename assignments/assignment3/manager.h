#ifndef MANAGER_H
#define MANAGER_H 

#include <iostream>
#include <fstream>

#include "airport.h"
#include "bintree.h"

class Manager {
private:
    enum class Action {
        PRINT_ALL     = 0,
        CHECK_FLIGHT  = 1,
        ADD_FLIGHT    = 2,
        CANCEL_FLIGHT = 3,
        TAKE_OFF      = 4,
        STATS         = 5,
        TAKE_OFF_ALL  = 6,
        QUIT          = 7
    };

    int num_airports = 0;         // number of airports
    Airport* airports = nullptr;  // airport array

    int populate(std::ifstream& fin);
    int init();
    Action get_menu_choice() const;
    Airport* airport_name_search(const std::string_view airport_name) const;
    Airport& read_input_airport(const std::string_view prompt) const;
    Airport read_new_destination(const Airport& start_airport) const;
    Flight& populate_new_flight(Flight& new_flight,
                                const Airport& start_airport) const;
    int get_num_incoming_flights(const Airport& dest) const;
    int launch_all_flights(Flight* flights_in_air);
    void land_all_flights(Flight* flights_in_air,
                          const int num_flights_in_air) const;
    void sync_file() const;
    

public:
    Manager();
    ~Manager();

    void print_all() const;
    void check_flight_control() const;
    void add_flight_control() const;
    void cancel_flight_control() const;
    void take_off_control() const;
    void stats_control() const;
    void take_off_all_control();

    void execute_action(const Action action_choice);
    void run();

};

#endif

