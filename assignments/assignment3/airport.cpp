#include <iostream>

#include "airport.h"
#include "input.h"
#include "flight.h"

using namespace std;


Airport::Airport() = default;


Airport::Airport(const int capacity)
  : capacity(capacity),
    flights(new Flight[capacity]) {
}


Airport::Airport(const Airport& other) {
    set_name(other.get_name());
    set_num_flights(other.get_num_flights());
    set_capacity(other.get_capacity());
    set_flights(other.get_flights(), this->num_flights);
}


Airport& Airport::operator=(const Airport other) {
    if (this == &other) {
        return *this;
    }

    if (this->flights) {
        delete[] this->flights;
        this->flights = nullptr;
    }

    set_name(other.get_name());
    set_num_flights(other.get_num_flights());
    set_capacity(other.get_capacity());
    set_flights(other.get_flights(), this->num_flights);
    return *this;
}


Airport::~Airport() {
    if (flights) {
        delete[] flights;
        flights = nullptr;
    }
}


string Airport::get_name() const { return name; }


int Airport::get_num_flights() const { return num_flights; }


int Airport::get_capacity() const { return capacity; }


Flight *Airport::get_flights() const { return flights; }


Flight& Airport::get_flight(const int idx) const { return flights[idx]; }


bool Airport::at_capacity() const { return num_flights == capacity; }


void Airport::set_name(const string_view new_name) {
    this->name = new_name;
    return;
}


void Airport::set_num_flights(const int new_num_flights) {
    this->num_flights = new_num_flights;
    return;
}


void Airport::set_capacity(const int new_capacity) {
    this->capacity = new_capacity;
    return;
}


void Airport::set_flights(const Flight* new_flights,
                          const int new_num_flights) {
    this->flights = new Flight[new_num_flights];
    for (int flight_index {0}; flight_index < new_num_flights; ++flight_index) {
        this->flights[flight_index] = new_flights[flight_index];
    }

    return;
}


void Airport::populate_airport(ifstream& fin){
    fin >> name >> num_flights >> capacity;
    flights = new Flight[capacity];
    for (Flight* flight {flights}; flight < flights + num_flights; ++flight) {
        flight->populate_flight(fin);
    }

    return; 
}


void Airport::write_to_file(ofstream& fout) const {
    fout << name << ' ' << num_flights << ' ' << capacity << '\n';

    for (const Flight* flight {flights}; flight < flights + num_flights;
         ++flight) {
        flight->write_to_file(fout);
    }
}


int Airport::add_flight(const Flight& new_flight) {
    if (at_capacity()) {
        cerr << "Error: airport " << this->name << " << is at capacity."
                     "Unable to add new flight.\n";
        return EXIT_FAILURE;
    }

    flights[num_flights] = new_flight;
    ++num_flights;
    return EXIT_SUCCESS; 
}


Flight Airport::remove_a_flight(const int idx) {
    if (idx >= num_flights) {
        cerr << "Error: attempted to remove flight that does not exist "
                "from airport " << this->name << ".\n";
        return Flight();
    }

    const Flight removed = flights[idx];

    // shift remaining flights over <- O(n) :(
    for (Flight* flight {flights + idx}; flight < flights + num_flights - 1;
         ++flight) {
        *flight = *(flight + 1);
    }

    --num_flights;

    return removed;
}


void Airport::print_airport(const int num_incoming_flights) const {
    cout << "\n"
            "                   Airport " << name << "\n"
            "                 Capacity: " << capacity << "\n"
            "Current number of flights: " << num_flights << "\n"
            "  Number Incoming flights: " << num_incoming_flights << '\n';
    
    return; 
}


void Airport::print_flights() const {
    for (const Flight* flight {flights}; flight < flights + num_flights;
         ++flight) {
        flight->print_flight();
    }
}


Flight* Airport::flight_number_search(string_view flight_num) const {
    for (Flight* flight {flights}; flight < flights + num_flights;
         ++flight) {
        if (flight->get_flight_num() == flight_num) {
            return flight;
        }
    }

    // no match found
    return nullptr;
}


int Airport::get_num_flights_to_dest(const Airport& dest) const {
    int num_flights_to_dest {0};
    for (const Flight* flight {flights}; flight < flights + num_flights;
         ++flight) {
        if (flight->get_dest() == dest.get_name()) {
            ++num_flights_to_dest;
        }
    }

    return num_flights_to_dest;
}


int Airport::read_flight_index() const {
    
    string* flight_numbers = new string[num_flights];
    for (int flight_index {0}; flight_index < num_flights; ++flight_index) {
        const Flight& flight {flights[flight_index]};
        flight_numbers[flight_index] = flight.get_flight_num();
    }

    const string* flight_number_menu
            = const_cast<const string*>(flight_numbers);
    const int selection {read_menu_selection(flight_number_menu, num_flights,
                                             "Select flight number: ")};

    delete[] flight_numbers;
    return selection;
}


Flight& Airport::read_input_flight() const {
    return flights[read_flight_index()];
}

