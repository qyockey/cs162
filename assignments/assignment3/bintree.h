#ifndef BINTREE_H
#define BINTREE_H

#include "queue.h"

template<typename T>
class Bintree {

private:


    using Node = struct Node {
        const T* value;
        Node* left;
        Node* right;


        Node() : value(nullptr), left(nullptr), right(nullptr) {
        }


        Node(T* val) : value(val), left(nullptr), right(nullptr) {
        }

    };


    Node* trunk;


    void add(T* leaf, Node*& start) {
        if (!start) {
            start = new Node;
            start->value = leaf;
            return;
        }
        if (leaf < start->value) {
            add(leaf, start->left);
        } else {
            add(leaf, start->right);
        }
        return;
    }


    void remove(const T* leaf, Node* start) {
        if (!start) {
            return;
        }

        if (leaf == start->value) {
            delete_node(start);
            return;
        }

        if (leaf < start->value) {
            remove(leaf, start->left);
        } else {
            remove(leaf, start->right);
        }
        return;
    }



    const T* search(int (*match)(), Node* start) {
        if (!start) {
            return nullptr;
        }

        if (match(start->value) == 0) {
            return start->value;
        }

        if (match(start->value) < 0) {
            return search(match, start->left);
        } else {
            return search(match, start->right);
        }
    }



    void delete_node(Node*& node) {
        delete node;
        node = nullptr;
        return;
    }



    void delete_tree(Node* start) {
        if (!start) {
            return;
        }

        if (start->left) {
            delete_node(start->left);
        }

        if (start->right) {
            delete_node(start->right);
        }

        delete_node(start);
        return;
    }


public:


    Bintree() : trunk(nullptr) {
    }
    

    Bintree(const T* start) : trunk(start) {
    }


    ~Bintree() {
        delete_tree();
    }


    bool empty() {
        return trunk == nullptr;
    }


    void delete_tree() {
        delete_tree(trunk);
        return;
    }


    void add(T* leaf) {
        add(leaf, trunk);
        return;
    }


    void remove(const T* leaf) {
        remove(leaf, trunk);
        return;
    }


    const T* search(int (*match)()) {
        return search(match, trunk);
    }


    bool contains(int (*match)(void* pattern)) {
        return search(match, trunk) != nullptr;
    }


    void treewalk(void (*action)(const T* leaf)) {
        if (!trunk) {
            return;
        }

        Queue<Node> bfs {};
        bfs.push(trunk);

        // breadth-first traversal of tree
        while (!bfs.empty()) {
            Node* node {bfs.pop()};
            action(node->value);

            if (node->left) {
                bfs.push(node->left);
            }
            if (node->right) {
                bfs.push(node->right);
            }
        }
        return;
    }


    void treewalk(void (*action)(T* leaf)) {
        if (!trunk) {
            return;
        }

        Queue<Node> bfs {};
        bfs.push(trunk);

        // breadth-first traversal of tree
        while (!bfs.empty()) {
            Node* node {bfs.pop()};
            action(node->value);

            if (node->left) {
                bfs.push(node->left);
            }
            if (node->right) {
                bfs.push(node->right);
            }
        }
        return;
    }

};

#endif

