#include "airport.h"
#include "bintree.h"
#include "input.h"
#include "manager.h"

#define AIRPORTS_FILE_NAME "airport.txt"
#define LOG_FILE_NAME "log.txt"

using namespace std;


Manager::Manager() = default;


Manager::~Manager() {
    if (airports) {
        delete[] airports;
        airports = nullptr;
    }
}


int Manager::populate(ifstream& fin) {
    fin >> num_airports;
    airports = new Airport[num_airports];
    if (!airports) {
        return EXIT_FAILURE;
    }

    for (Airport* airport {airports}; airport < airports + num_airports;
         ++airport) {
        airport->populate_airport(fin);
    }
    return EXIT_SUCCESS; 
}


int Manager::init() {
    ifstream fin(AIRPORTS_FILE_NAME);

    if (!fin.is_open() || fin.fail()) {
        cerr << "Error opening airport info file " AIRPORTS_FILE_NAME ".\n";
        return EXIT_FAILURE;
    }

    Manager::populate(fin);
    return EXIT_SUCCESS;
}


Manager::Action Manager::get_menu_choice() const {
    const static string actions[] = {
        "View all Airports & Flights info",
        "Check flight info",
        "Add a new flight",
        "Cancel a flight",
        "Take off a flight",
        "Print airport stats",
        "Take off all flights",
        "Quit"
    };
    constexpr static int num_actions = 8;

    return static_cast<Action>(read_menu_selection(actions, num_actions,
                                                   "Select an action: "));
}


Airport* Manager::airport_name_search(const std::string_view airport_name)
        const {
    for (Airport* airport {airports}; airport < airports + num_airports;
         ++airport) {

        if (airport->get_name() == airport_name) {
            return airport;
        }
    }

    // no match found
    return nullptr;
}


Airport Manager::read_new_destination(const Airport& start_airport) const {
    while (true) {
        const Airport& dest {read_input_airport("Select destination airport: ")};
        if (&dest == &start_airport) {
            cout << "Destination cannot be the same as starting airport. "
                    "Select another airport";
        } else if (dest.at_capacity()) {
            cout << "Airport " << dest.get_name() << " is at capacity. "
                    "Select another airport";
        } else {
            return dest;
        }
    }
}


Flight& Manager::populate_new_flight(Flight& new_flight,
                                     const Airport& start_airport) const {
    new_flight.set_curr_loc(start_airport.get_name());
    new_flight.set_flight_num(read_string("Enter flight number: "));

    Airport dest {read_new_destination(start_airport)};
    new_flight.set_dest(dest.get_name());

    const int num_pilots {read_positive_int("Enter number of pilots: ")};
    new_flight.set_num_pilots(num_pilots);
    string* pilots = new string[num_pilots];
    for (int pilot_num {0}; pilot_num < num_pilots; ++pilot_num) {
        pilots[pilot_num] = read_string("Enter pilot "
                                        + to_string(pilot_num + 1)
                                        + " name: " );
    }
    new_flight.set_pilots(pilots);

    return new_flight;
}


int Manager::get_num_incoming_flights(const Airport& dest) const {
    int num_incoming_flights {0};
    for (const Airport* airport {airports}; airport < airports + num_airports;
         ++airport) {

        if (airport != &dest) {
            num_incoming_flights += airport->get_num_flights_to_dest(dest);
        }
    }

    return num_incoming_flights;
}


int Manager::launch_all_flights(Flight* flights_in_air) {
    int num_flights_in_air {0};
    for (Airport* airport {airports}; airport < airports + num_airports;
         ++airport) {

        // remove flights in reverse order for faster removal
        for (int flight_index {airport->get_num_flights() - 1};
             flight_index >= 0; --flight_index) {
            const Flight& flight {airport->get_flight(flight_index)};

            // only take off flights if not already at destination
            if (flight.get_curr_loc() != flight.get_dest()) {
                flights_in_air[num_flights_in_air] =
                        airport->remove_a_flight(flight_index);
                ++num_flights_in_air;
            }
        }
    }

    return num_flights_in_air;
}


void Manager::land_all_flights(Flight* flights_in_air,
                               const int num_flights_in_air) const {
    ofstream log_file;
    log_file.open(LOG_FILE_NAME, ios::app);

    for (Flight* flight {flights_in_air};
         flight < flights_in_air + num_flights_in_air; ++flight) {
        Airport* dest {airport_name_search(flight->get_dest())};
        if (!dest) {
            cerr << "Error: invalid destination airport " << dest->get_name()
                 << " in flight " << flight->get_flight_num() << '\n';
            return;
        }

        log_file << "Flight " << flight->get_flight_num() << " is moved from "
                 << flight->get_curr_loc() << " to " << flight->get_dest()
                 << '\n';
        flight->set_curr_loc(dest->get_name());
        dest->add_flight(*flight);
    }

    log_file.close();
    return;
}


void Manager::sync_file() const {
    ofstream airport_file;
    airport_file.open(AIRPORTS_FILE_NAME, ios::trunc);

    airport_file << num_airports << '\n';
    for (const Airport* airport {airports}; airport < airports + num_airports;
         ++airport) {
        airport->write_to_file(airport_file);
    }

    airport_file.flush();
    airport_file.close();
    return;
}


void Manager::print_all() const {
    for (const Airport* airport {airports}; airport < airports + num_airports;
         ++airport) {
        int num_incoming_flights = get_num_incoming_flights(*airport);
        airport->print_airport(num_incoming_flights);
        airport->print_flights();
    }
    return;
}


Airport& Manager::read_input_airport(const string_view prompt) const {
    
    string* airport_names = new string[num_airports];
    for (int airport_index {0}; airport_index < num_airports; ++airport_index) {
        const Airport& airport {airports[airport_index]};
        airport_names[airport_index] = airport.get_name();
    }

    const int selection {read_menu_selection(airport_names, num_airports,
                                             prompt)};

    delete[] airport_names;
    return airports[selection];
}


void Manager::check_flight_control() const {
    cout << "Enter flight number to search for: ";
    string flight_num;
    getline(cin, flight_num);

    bool match_found = false;
    for (const Airport* airport {airports}; airport < airports + num_airports;
         ++airport) {

        const Flight* flight_match {airport->flight_number_search(flight_num)};
        if (flight_match) {
            match_found = true;
            flight_match->print_flight();
        }
    }

    if (!match_found) {
        cerr << "Error: No flight found with flight number " << flight_num
             << ".\n";
    }
    
    return; 
}


void Manager::add_flight_control() const {
    Airport& start_airport {read_input_airport("Select starting airport: ")};

    if (start_airport.at_capacity()) {
        cerr << "Airport " << start_airport.get_name() << " is at capacity.\n"
                "Unable to add more flights.\n";
        return;
    }

    Flight new_flight;
    populate_new_flight(new_flight, start_airport);
    start_airport.add_flight(new_flight);
    return; 
}


void Manager::cancel_flight_control() const {
    Airport& airport {read_input_airport("Select airport containing flight to "
                                         "remove: ")};
    const int remove_index {airport.read_flight_index()};
    airport.remove_a_flight(remove_index);
    return;
}


void Manager::take_off_control() const {
    Airport& start_airport {read_input_airport("Select airport containing "
                                               "flight to take off: ")};
    const int flight_index {start_airport.read_flight_index()};
    Flight& flight {start_airport.get_flight(flight_index)};
    string destination_name {flight.get_dest()};
    Airport* p_dest {airport_name_search(destination_name)};

    if (!p_dest) {
        cerr << "Error: invalid destination airport " << destination_name
             << " in flight " << flight.get_flight_num() << '\n';
        return;
    }

    // set location change before deep copy, reset if move fails
    flight.set_curr_loc(p_dest->get_name());
    if (p_dest->add_flight(flight) == EXIT_SUCCESS) {
        start_airport.remove_a_flight(flight_index);
    } else {
        flight.set_curr_loc(start_airport.get_name());
    }
    
    return; 
}


void Manager::stats_control() const {
    for (const Airport* airport {airports}; airport < airports + num_airports;
         ++airport) {

        int num_incoming_flights = get_num_incoming_flights(*airport);
        airport->print_airport(num_incoming_flights);
    }
    
    return; 
}


void Manager::take_off_all_control() {
    int total_num_flights {0};
    for (const Airport* airport {airports}; airport < airports + num_airports;
         ++airport) {
        if (get_num_incoming_flights(*airport) > airport->get_capacity()) {
            cerr << "Error: More flights to airport " << airport->get_name()
                 << " than it can hold.\n"
                    "Taking off all flights operation cancelled.\n";
            return;
        }
        total_num_flights += airport->get_num_flights();
    }
    
    Flight* flights_in_air {new Flight[total_num_flights]};
    int num_flights_in_air {launch_all_flights(flights_in_air)};
    land_all_flights(flights_in_air, num_flights_in_air);

    cout << "All flights successfully arrived at their destinations.\n"
            "Check " LOG_FILE_NAME " for details.\n";

    delete[] flights_in_air;
    return;
}


/* Over 15 lines: cleanest way to select functions to execute given action */
void Manager::execute_action(const Action selected_action) {
    switch (selected_action) {
        case Action::PRINT_ALL:
            print_all();
            break;

        case Action::CHECK_FLIGHT:
            check_flight_control();
            break;

        case Action::ADD_FLIGHT:
            add_flight_control();
            sync_file();
            break;

        case Action::CANCEL_FLIGHT:
            cancel_flight_control();
            sync_file();
            break;

        case Action::TAKE_OFF:
            take_off_control();
            sync_file();
            break;

        case Action::STATS:
            stats_control();
            break;

        case Action::TAKE_OFF_ALL:
            take_off_all_control();
            sync_file();
            break;

        case Action::QUIT:
            break;

    }
    return;
}


void Manager::run() {
    cout << "\nWelcome to Flight Manager!!\n";
    if (Manager::init() != EXIT_SUCCESS) {
        return;
    }

    Action selected_action;
    do {
        selected_action = get_menu_choice();
        execute_action(selected_action);
    } while (selected_action != Action::QUIT);
    
    cout << "Bye!\n\n";
    return;
}

