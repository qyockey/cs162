#ifndef FLIGHT_H
#define FLIGHT_H 

#include <iostream>
#include <string>
#include <fstream>

#include "bintree.h"

class Flight {
private: 
    std::string flight_num = std::string();  // flight number
    std::string curr_loc = std::string();    // current airport
    std::string dest = std::string();        // destination 
    int num_pilots = 0;                      // number of pilots
    std::string* pilots = nullptr;           // array of pilots

public:
    Flight();
    Flight(const int num_pilots);
    Flight(const Flight& other);
    Flight& operator=(const Flight& other);
    ~Flight();

    void populate_flight(std::ifstream& fin);
    void write_to_file(std::ofstream& fout) const;
    void print_flight() const;
    
    void set_flight_num(const std::string_view flight_num);
    void set_curr_loc(const std::string_view current_location);
    void set_dest(const std::string_view destination);
    void set_num_pilots(const int);
    void set_pilots(std::string* pilots);

    std::string get_flight_num() const;
    std::string get_curr_loc() const;
    std::string get_dest() const;
    int get_num_pilots() const;
    std::string* get_pilots() const;

};

#endif


